import { Validators } from '@angular/forms';
import { Language } from '@app/interfaces';
import { UpsWarningComponent } from '@app/static-templates/ups-warning/ups-warning.component';
import { YdmPopupComponent } from '@app/static-templates/ydm-popup/ydm-popup.component';

const PACKAGE_DETAIL_TEMPLATE: object = {

  weight: ['', Validators.required],
  quantity: ['', Validators.required],
  length: [''],
  width: [''],
  height: [''],
  description: ['test'],
};

const POPUP_TYPES = {
  UPS: UpsWarningComponent,
  YDM: YdmPopupComponent,
};

const DEFAULT_LANGUAGE: Language = {
  code: 'en',
  isRtl: false,
  nativeCode: 'en'
};


export {PACKAGE_DETAIL_TEMPLATE, DEFAULT_LANGUAGE, POPUP_TYPES};
