
import { Slide } from '@app/interfaces';

//const PARTNERS: string[] = ['ydm', 'usps', 'dhl', 'fedex', 'ups', 'tnt', 'gett'];
const PARTNERS: string[] = ['ydm', 'usps', 'dhl',  'ups',  'gett'];
const SLIDES: Slide[] = [
  {
    src: 'assets/img/slider/slide1.png',
    heading: '220 destinations',
    subheader: 'from anywhere to anywhere',
    link: 'ship now',
    description: `
    Shippiz gives your business extensive international coverage of over 220 countries and territories.
    Covering 99% of global GDP. Shipping is done through the leading courier and shipping companies that
    include a wide range of shipping services.

    Shippiz allows you to export, sell and make a profit. We made
    the negotiations and contract with the shipping companies, you just stay connected to the Shippiz website
    and start selling anywhere in the world
  `},
  {
    src: 'assets/img/slider/slide2.png',
    heading: '70% off',
    subheader: 'up to 70% off the price list',
    link: 'ship now',
    description: `
    Thanks to Shippiz’s exclusive commercial agreements, you can send packages and
    documents from anywhere in the world at up to 70% off the price list, thereby increasing
    your business’s profits. Shippiz has made savings and promotion of shipping options for
    small and medium businesses its calling card, and stands by its promise to provide the best
    service for the best price.
  `},

  {
    src: 'assets/img/slider/slide3.png',
    heading: 'freight',
    subheader: 'special service for large shipments',
    link: 'contact us',
    description: `
    Furthering our commitment to cater to the small to medium business, Shippiz Service has
    contracted agents at several locations around the world to offer you competitive and
    discounted offers for shipping heavy cargo to any destination in the world. Shippiz Service
    can ease the import and export of goods, personal items, irregular cargo, hazardous
    materials, crafts, valuable items and more. The service includes pick-up from the customers
    address, handling local customs documents, and delivery to the destination.
  `},

  {
    src: 'assets/img/slider/slide4.png',
    heading: 'fba amazon',
    subheader: 'the world leader for amazon sellers',
    link: 'contact us',
    description: `
    Shippiz: The best way to send your parcel!
    Shipment of goods to Amazon is not a regular shipment. Amazon has an extensive set of
    rules and regulations and any deviation can complicate and delay the entry of your goods
    into Amazon’s warehouses, as well as compromise your quality score, resulting in decreased
    sales.
    Prior knowledge and adherence to the guidelines will help you through the shipping process
    smoothly and problem-free.
  `},

  {
    src: 'assets/img/slider/slide5.png',
    heading: 'warehouse',
    subheader: 'international logistics warehouses services',
    link: 'contact us',
    description: `
    Shippiz Services has contracted with several logistics warehouses around the world to help
    businesses who need to store their goods from time to time, especially for eCommerce
    merchants looking to expand and do drop-shipping. We will be your ambassadors in the
    countries where you want to trade, placing your goods with us. We will take care of
    everything. All you worry about is sales - everything else is on us!
  `},

  {
    src: 'assets/img/slider/slide6.png',
    heading: 'order few seconds',
    subheader: '90 seconds for ordering',
    link: 'ship now',
    description: 'not spacified',
  },
];


export {PARTNERS, SLIDES};
