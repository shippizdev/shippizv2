import { Router, NavigationStart, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LanguageObserver, AuthFormStateObserver, CurrencyObserver, CurrencyHandler } from '@app/utils/observers';
import { BehaviorSubject } from 'rxjs';
import { Language, AuthFormState, User, Processor } from '@app/interfaces';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { isEmail, passwordConfirm } from '@app/utils/validators';
import { AuthService, CacheService, BackendService, LocalStorageService } from '@app/services';
import { RowInsertion } from '@app/animations';
import { MatDialog } from '@angular/material';
import { ResetPasswordComponent } from '@app/components/reset-password/reset-password.component';
import { CURRENCY_TOKEN, ENVIRONMENT_TOKEN } from '@app/constants';
import { API } from '@app/classes';
import { InvoiceManagerService } from '@app/services/invoice-manager.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [RowInsertion]
})

export class HeaderComponent implements OnInit {

  envControl: FormControl;
  isSidebarVisible = false;
  submitFailed = false;
  loginPasswordEncrypted = true;
  registerPasswordEncrypted = true;
  asyncErrorMsg: string;
  selectedLanguage: BehaviorSubject<Language>;
  authFormState: AuthFormState;
  loginForm: FormGroup;
  registrationForm: FormGroup;
  currentUser: User = null;
  currencyHandler = CurrencyHandler;
  currencyObserver = CurrencyObserver;
  currencyDisabled = false;
  isYdm: boolean;

  processors: Processor[] = [];
  uniqueProcessors: Processor[] = [];
  selectedProcessor: Processor;
  trackNumber = '';
  myTrackingNumbers: {number: string, processorId: number}[] = [];

  constructor(
    private invoiceMng: InvoiceManagerService,
    private store: LocalStorageService,
    private fb: FormBuilder,
    private auth: AuthService,
    private active: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private cache: CacheService,
    private backend: BackendService) { }

  selectLanguage(code: string, isRtl: boolean, nativeCode: string) {
    this.selectedLanguage.next({code, isRtl, nativeCode});
  }

  skipLogin() {
    AuthFormStateObserver.next({mode: null, skipped: true});
  }

  openLogin() {
    this.asyncErrorMsg = null;
    this.submitFailed = false;
    this.registrationForm.reset();
    AuthFormStateObserver.next({mode: 'login'});
  }

  openForgotPassword() {
    this.dialog.open(ResetPasswordComponent, {
      panelClass: ['cst-mat-dialog']
    });
  }

  openRegistration() {
    this.asyncErrorMsg = null;
    this.submitFailed = false;
    this.loginForm.reset();
    AuthFormStateObserver.next({mode: 'registration'});
  }

  closeAuthForm() {
    AuthFormStateObserver.next({mode: null});
  }

  openSideBar() {
    this.isSidebarVisible = true;
  }
  closeSideBar() {
    this.isSidebarVisible = false;
  }

  async register() {

    if (this.registrationForm.invalid) {
      return this.submitFailed = true;
    }

    try {
      await this.auth.register(this.registrationForm.value);
      this.closeAuthForm();
    } catch (err) {
      this.asyncErrorMsg = err;
    }
  }

  async login() {

    try {
      await this.auth.login(this.loginForm.value);
      this.closeAuthForm();
    } catch (err) {
      this.asyncErrorMsg = err;
    }
  }

  logout() {
    this.auth.logout();
  }


  get requiredError(): boolean {
    const controls = Object.values(this.registrationForm.controls);
    return this.submitFailed && controls.some(control => control.hasError('required'));
  }

  get dismatchError(): boolean {
    return this.submitFailed && this.registrationForm.hasError('dismatch');
  }

  get invalidPasswordError(): boolean {
    return this.submitFailed && this.registrationForm.get('password').hasError('minlength');
  }

  get invalidEmailError(): boolean {
    return this.submitFailed && this.registrationForm.get('email').hasError('invalidEmail');
  }






  setTrackNumber(value) {
    this.trackNumber = value;
    const id = this.myTrackingNumbers.find(item => item.number === value).processorId;
    this.selectedProcessor = this.processors.find(proc => proc.processorId === id);
  }


  getTrack() {
    const url = this.selectedProcessor.trackingURL;
    window.open(url + this.trackNumber);
  }

  openBlankInvoice() {
    this.invoiceMng.openInvoice(null);
  }




  ngOnInit() {




    this.envControl = new FormControl();
    this.envControl.valueChanges.subscribe(_env => {
      this.store.set<string>(ENVIRONMENT_TOKEN, _env);
      API.setEnvironment(_env);
    });

    this.envControl.setValue(API.__enviorment);

    const currentCurrency = this.store.get<string>(CURRENCY_TOKEN);

    if (currentCurrency !== null) {
      this.currencyHandler.next(currentCurrency);
    }

    this.currencyHandler.subscribe(currency => {
      this.store.set<string>(CURRENCY_TOKEN, currency);
    });

    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.currencyDisabled = event.url.includes('payment_details') || event.url.includes('confirmation');
      }

      if (event instanceof NavigationEnd) {
        const subdomain = document.location.href.replace('https://', '').split('.')[0];
        this.isYdm = subdomain === 'ydm' || this.router.url.includes('ydmpickupform');
      }
    });

    this.cache.getProcessorType().then(processors => {
      const items = processors as Processor[];
      this.processors = items;
      this.uniqueProcessors = items.filter((item, index) => items.findIndex(_item => _item.logo === item.logo) === index);
      this.selectedProcessor = this.processors[0];
    });

    this.auth.currentUser.subscribe(user => {
      this.currentUser = user;

      if (user) {
        this.backend.execute('GetUserOrders', {sessionId: user.sessionId}).then(res => {
          const trackNumbers = res.data[0].map(order => ({number: order.shippingAuthorization, processorId: order.processorId}));
          this.myTrackingNumbers = trackNumbers;
        });
      } else {
        this.myTrackingNumbers = [];
      }
    });

    AuthFormStateObserver.subscribe(state => {
      this.authFormState = state;
    });
    this.selectedLanguage = LanguageObserver;

    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.registrationForm = this.fb.group({
      email: ['', [Validators.required, isEmail]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      passwordConfirm: ['', [Validators.required]],
    }, {validators: passwordConfirm});



    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.isSidebarVisible = false;
      }
    });

  }

}
