import { Component, OnInit } from '@angular/core';
import { LocalStorageService, CacheService } from '@app/services';
import { PickupResponseData, CompareResponseData, Processor } from '@app/interfaces';
import { PICKUP_DATA_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN } from '@app/constants';
import { MatDialog } from '@angular/material';
import { UpsWarningComponent } from '@app/static-templates/ups-warning/ups-warning.component';
import { InvoiceManagerService } from '@app/services/invoice-manager.service';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss']
})
export class ConfirmationPageComponent implements OnInit {

  data: PickupResponseData;
  msg: string;

  constructor(
    private invoiceMng: InvoiceManagerService,
    private cache: CacheService,
    private store: LocalStorageService,
    private dialog: MatDialog) { }

  async ngOnInit() {
    this.data = this.store.get<PickupResponseData>(PICKUP_DATA_TOKEN);
    const processorId = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN).processorId;
    const processor = await this.cache.getProcessorType(+processorId) as Processor;
    const values = {
      processorName: processor.originalName,
      pickupId: this.data.pickupId,
    };

    // if (this.data.isShowPopup) {
    //   this.dialog.open(UpsWarningComponent, {
    //     data: values,
    //     width: '800px'
    //   });
    // } else {
    // }
    this.msg = this.data.instruction;
  }


  openInvoice() {
    this.invoiceMng.openInvoice(this.data.pickupId);
  }

}
