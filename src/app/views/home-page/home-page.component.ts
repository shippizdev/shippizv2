import { NotificationService } from '../../services/notification.service';
import { Component, OnInit } from '@angular/core';
import { PARTNERS, INTEGER_REGEXP, FLOAT_REGEXP, PACKAGE_DETAIL_TEMPLATE, SLIDES, ORDER_DATA_TOKEN, LOCATIONS_TOKEN } from '@app/constants';
import { GooglePlace, Slide, CompareRequest, Locations } from '@app/interfaces';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { GoogleAutocompleteService, ConverterService, LocalStorageService } from '@app/services';
import { Router } from '@angular/router';
import { RowInsertion } from '@app/animations';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [RowInsertion]
})
export class HomePageComponent implements OnInit {


  selectedSlideIndex: number;
  selectedTabIndex: number;
  partners: string[];
  intReg: RegExp;
  floatReg: RegExp;
  compareForm: FormGroup;
  locations: Locations;

  carouselConfig: any;
  sliderData: Slide[];

  constructor(
    private builder: FormBuilder,
    private googleAutocomplete: GoogleAutocompleteService,
    private converter: ConverterService,
    private store: LocalStorageService,
    private router: Router,
    private notification: NotificationService) {}


  setShippingTypeId(shippingTypeId: 1 | 2 | 4) {
    this.compareForm.patchValue({shippingTypeId});
  }

  addPackage(): void {
    this.packageDetails.push(this.builder.group(PACKAGE_DETAIL_TEMPLATE));
  }

  removePackage(index: number): void {
    this.packageDetails.removeAt(index);
  }

  removeAllPackageValues() {
    const packages = this.compareForm.get('packageDetails') as FormArray;
    packages.controls.forEach(pack => {
      pack.reset();
    });
  }


  get org(): FormGroup {
    return this.compareForm.get('origin') as FormGroup;
  }

  get dest(): FormGroup {
    return this.compareForm.get('destination') as FormGroup;
  }

  get shippingTypeId(): FormControl {
    return this.compareForm.get('shippingTypeId') as FormControl;
  }

  get packageDetails(): FormArray {
    return this.compareForm.get('packageDetails') as FormArray;
  }


  setPlace(target: string, place: GooglePlace) {
    this[target].patchValue(this.googleAutocomplete.retrieveAreas(place));
    this.locations[target] = place.formatted_address;
  }


  setPostalCode(target: string, postalCode: string) {
    if (postalCode) {
      this[target + 'PostalCodeNotFound'] = false;
      this[target].patchValue({postalCode});
    } else {
      this[target].patchValue({postalCode: ''});
    }
  }

  compare() {
    const controls = this.packageDetails.controls;

    if (this.compareForm.get('origin').invalid || this.compareForm.get('destination').invalid) {
      this.notification.errorNotification('Fill both endpoints');
      return;
    }


    if (this.packageDetails.controls.every(item => item.invalid)) {
      this.notification.errorNotification('At least one package detail should be present');
      return;
    }

    controls.forEach((item, i) => {
      if (item.invalid) {
        this.packageDetails.removeAt(i);
        i--;
      }
    });

    const json = this.converter.convertToCompareRequest(this.compareForm.value);
    this.store.set<CompareRequest>(ORDER_DATA_TOKEN, json);
    this.store.set<CompareRequest>(ORDER_DATA_TOKEN, json, true);
    this.store.set<Locations>(LOCATIONS_TOKEN, this.locations);
    this.store.set<Locations>(LOCATIONS_TOKEN, this.locations, true);

    this.router.navigate(['ship']);
  }

  applyLastOrder(): void {

    const lastOrder = this.store.get<CompareRequest>(ORDER_DATA_TOKEN) || this.store.get<CompareRequest>(ORDER_DATA_TOKEN, true);

    if (lastOrder) {
      const packageQuantity = lastOrder.packageDetails.length;

      for (let i = 1; i < packageQuantity; i++) {
        this.packageDetails.push(this.builder.group(PACKAGE_DETAIL_TEMPLATE));
      }

      this.compareForm.patchValue(this.converter.convertToCompareForm(lastOrder));

    }

    this.locations = this.store.get<Locations>(LOCATIONS_TOKEN) || this.store.get<Locations>(LOCATIONS_TOKEN, true) || {org: '', dest: ''};
  }

  flipEndpoints() {

    this.compareForm.patchValue({
      origin: this.dest.value,
      destination: this.org.value,
    });

    [this.locations.org, this.locations.dest] = [this.locations.dest, this.locations.org];
    // [this.orgPostalCodeNotFound, this.destPostalCodeNotFound] = [this.destPostalCodeNotFound, this.orgPostalCodeNotFound];
  }
  clicked() {
    if (this.compareForm.get('measurementUnits').value === 1) {
      this.compareForm.get('measurementUnits').setValue(2);
    } else {
      this.compareForm.get('measurementUnits').setValue(1);
    }
  }


  ngOnInit() {

    this.carouselConfig = {
      grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
      slide: 1,
      speed: 300,
      point: {
        visible: true
      },
      load: 2,
      loop: true,
      touch: true
    };

    this.sliderData = SLIDES;

    this.selectedTabIndex = 1;
    this.partners = PARTNERS;
    this.intReg = INTEGER_REGEXP;
    this.floatReg = FLOAT_REGEXP;

    this.compareForm = this.builder.group({
      origin: this.builder.group({
        country: ['', Validators.required],
        city: ['', Validators.required],
        state: [''],
        street: [''],
        houseNumber: [''],
        postalCode: [''],
      }),

      destination: this.builder.group({
        country: ['', Validators.required],
        city: ['', Validators.required],
        state: [''],
        street: [''],
        houseNumber: [''],
        postalCode: [''],
      }),

      shippingTypeId: [1],
      measurementUnits: [1],
      currency: ['USD'],

      packageDetails: this.builder.array([
        this.builder.group(PACKAGE_DETAIL_TEMPLATE)
      ])
    });


    this.applyLastOrder();
  }
}
