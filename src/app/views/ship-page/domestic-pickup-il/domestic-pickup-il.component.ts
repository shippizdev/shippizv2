import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LocalStorageService, ConverterService, BackendService, GoogleAutocompleteService } from '@app/services';
import { CompareRequest, Locations, CompareResponseData, ShipmentForm, GooglePlace } from '@app/interfaces';
import {
  ORDER_DATA_TOKEN,
  LOCATIONS_TOKEN,
  SELECTED_PROCESSOR_DATA_TOKEN,
  NORMALIZED_ORDER_DATA_TOKEN,
  SHIPMENT_DATA_TOKEN
} from '@app/constants';
import { NotificationService } from '@app/services/notification.service';
import { Router } from '@angular/router';
import { RowInsertion } from '@app/animations';

@Component({
  selector: 'app-domestic-pickup-il',
  templateUrl: './domestic-pickup-il.component.html',
  styleUrls: ['./domestic-pickup-il.component.scss'],
  animations: [RowInsertion],
})
export class DomesticPickupILComponent implements OnInit {

  ydmExtraChargeForLocation = new FormControl(false);
  requesting = false;
  isSubmitFailed = false;
  pickupForm: FormGroup;
  order: CompareRequest;
  locations: Locations;
  selectedService: CompareResponseData;
  normalizedOrder: CompareRequest;

  orgAddressVisible = false;
  destAddressVisible = false;

  unfoundFragments: string[] = [];

  shipperAddress = new FormControl('');
  consigneeAddress = new FormControl('');


  constructor(
    private googleAuto: GoogleAutocompleteService,
    private router: Router,
    private notification: NotificationService,
    private backend: BackendService,
    private builder: FormBuilder,
    private store: LocalStorageService,
    private converter: ConverterService) { }


  async setAddress(place: GooglePlace, target: 'shipper' | 'consignee'): Promise<void> {
    const street = this.googleAuto.getStreet(place);
    const houseNumber = this.googleAuto.getHouseNumber(place);
    const postalCode = await this.googleAuto.retrievePostalCode(place);


    if (street) {
      this[target + 'Data'].get('street').setValue(street);
    } else if (!this[target + 'Data'].get('street').value) {
      this.unfoundFragments.push(target + 'Street');
    }

    if (houseNumber) {
      this[target + 'Data'].get('houseNumber').setValue(houseNumber);
    } else if (!this[target + 'Data'].get('houseNumber').value) {
      this.unfoundFragments.push(target + 'House');
    }

    if (postalCode) {
      this[target + 'Data'].get('postalCode').setValue(postalCode);
    } else if (!this[target + 'Data'].get('postalCode').value) {
      this.unfoundFragments.push(target + 'PostalCode');
    }

    setTimeout(() => {
      this.unfoundFragments = [];
    }, 2000);


  }

  get shipperData(): FormGroup {
    return this.pickupForm.get('shipperData') as FormGroup;
  }

  get consigneeData(): FormGroup {
    return this.pickupForm.get('consigneeData') as FormGroup;
  }

  ngOnInit() {

    this.order = this.store.get<CompareRequest>(ORDER_DATA_TOKEN);
    this.locations = this.store.get<Locations>(LOCATIONS_TOKEN);
    this.selectedService = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);


    this.pickupForm  = this.builder.group({

      shipperData: this.builder.group({
        country: [''],
        state : [''],
        city: ['', Validators.required],
        name : [''],
        street : ['', Validators.required],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : ['', Validators.required],
        phone : ['', [Validators.required, Validators.minLength(8)]],
        email : ['', [Validators.required, Validators.email]],
        additionalInstructions: [''],
      }),

      consigneeData: this.builder.group({
        country: [''],
        state : [''],
        name : [''],
        city: ['', Validators.required],
        street : ['', Validators.required],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : ['', Validators.required],
        phone : ['', [Validators.required, Validators.minLength(8)]],
        email : ['', [Validators.required, Validators.email]],
        additionalInstructions: [''],
      }),

    });

    this.normalizedOrder = this.converter.normalizeOrder(this.order, this.selectedService);
    const hasChanged = this.store.compare<CompareRequest>(NORMALIZED_ORDER_DATA_TOKEN, this.normalizedOrder);
    this.pickupForm.patchValue(this.normalizedOrder);

    const shipmentDetails = this.store.get<ShipmentForm>(SHIPMENT_DATA_TOKEN);
    if (hasChanged && shipmentDetails) {
      this.pickupForm.patchValue(shipmentDetails);
    }

    const org = `${this.shipperData.get('city').value}, Israel, `;
    const dest = `${this.consigneeData.get('city').value}, Israel, `;

    this.shipperAddress.setValue(org);
    this.consigneeAddress.setValue(dest);


    this.shipperAddress.valueChanges.subscribe(value => {
      if (value.indexOf(org) === 0) {
      } else {
        if (org.indexOf(value) >= 0) {
          this.shipperAddress.setValue(org);
        } else {
          this.shipperAddress.setValue(org + value);
        }
      }
    });

    this.consigneeAddress.valueChanges.subscribe(value => {
      if (value.indexOf(dest) === 0) {
      } else {
        if (dest.indexOf(value) >= 0) {
          this.consigneeAddress.setValue(dest);
        } else {
          this.consigneeAddress.setValue(dest + value);
        }
      }
    });

  }

  async createShipment() {

    console.log(this.pickupForm.errors, this.pickupForm.get('consigneeData').errors);

    if (this.pickupForm.valid) {
      this.requesting = true;

      try {
        this.store.set<ShipmentForm>(SHIPMENT_DATA_TOKEN, this.pickupForm.value);
        const request = await this.converter.convertToShipmentRequest(this.pickupForm.value);

        request.clientJson.ydmExtraChargeForLocation = Number(this.ydmExtraChargeForLocation.value);
        const response = await this.backend.executeShipment(request);
        const data = response.data[0][0];
        this.router.navigate(['/ship', 'payment_details', data.pickupId]);
      } catch (error) {
        this.notification.errorNotification(error);
      } finally {
        this.requesting = false;
      }
    } else {
      this.isSubmitFailed = true;
    }

  }


}
