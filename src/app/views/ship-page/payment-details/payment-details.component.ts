import {
  Component,
  OnInit,
  ViewChildren,
  QueryList,
  ElementRef
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import {
  LocalStorageService,
  ConverterService,
  BackendService,
  AuthService,
  CacheService
} from "@app/services";
import { RowInsertion } from "@app/animations";
import {
  INTEGER_REGEXP,
  PICKUP_DATA_TOKEN,
  POPUP_TYPES,
  SELECTED_PROCESSOR_DATA_TOKEN
} from "@app/constants";
import {
  PickupResponseData,
  User,
  CompareResponseData,
  Processor
} from "@app/interfaces";
import { PaymentAutofill } from "@app/utils/observers";
import { filter, take } from "rxjs/operators";
import { MatDialog } from "@angular/material";

@Component({
  selector: "app-payment-details",
  templateUrl: "./payment-details.component.html",
  styleUrls: ["./payment-details.component.scss", "../detail-page-root.scss"],
  animations: [RowInsertion]
})
export class PaymentDetailsComponent implements OnInit {
  requesting = false;
  intReg: RegExp;
  isSubmitted = false;
  monthOptions: number[] = [];
  allMonthOptions: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  currentYearMothOptions: number[] = [];
  yearOptions: number[] = [];
  currentYear = new Date()
    .getFullYear()
    .toString()
    .slice(-2);
  errorMes: string;
  user: User;
  chargeFromBalance = new FormControl(false);

  paymentForm: FormGroup;
  constructor(
    private cache: CacheService,
    private dialog: MatDialog,
    private auth: AuthService,
    private builder: FormBuilder,
    private activeRoute: ActivatedRoute,
    private converter: ConverterService,
    private backend: BackendService,
    private store: LocalStorageService,
    private router: Router
  ) {}

  @ViewChildren("expDate")
  expDateControls: QueryList<ElementRef<HTMLSelectElement>>;

  get yearValue() {
    return this.expDateControls.last.nativeElement.value;
  }

  get monthValue() {
    return this.expDateControls.first.nativeElement.value;
  }

  async createPickup() {
    this.errorMes = null;
    if (this.paymentForm.invalid && this.chargeFromBalance.value === false) {
      this.isSubmitted = true;
      return;
    }

    const pickupId = this.activeRoute.snapshot.paramMap.get("pickupId");
    const request = this.converter.convertToPickupRequest(
      this.paymentForm.value,
      pickupId,
      this.chargeFromBalance.value
    );

    this.requesting = true;

    try {
      const response = await this.backend.executePickup(request);
      await this.auth.syncUser();
      this.store.set<PickupResponseData>(
        PICKUP_DATA_TOKEN,
        response.data[0][0]
      );

      const data = response.data[0][0];

      const { popupType, isNavigateToConfirmationPage } = data;

      const processorId = this.store.get<CompareResponseData>(
        SELECTED_PROCESSOR_DATA_TOKEN
      ).processorId;
      const processor = (await this.cache.getProcessorType(
        +processorId
      )) as Processor;
      const values = {
        processorName: processor.originalName,
        pickupId
      };

      if(POPUP_TYPES[popupType]){

        this.dialog.open(POPUP_TYPES[popupType], {
          data: values,
          width: "800px"
        });

      }



      PaymentAutofill.next(null);

      if (isNavigateToConfirmationPage) {
        this.router.navigate(["/confirmation"]);
      } else {
        this.router.navigate(["/home"]);
      }
    } catch (err) {
      this.errorMes = err;
      PaymentAutofill.next(this.paymentForm.value);
    } finally {
      this.requesting = false;
    }
  }

  setExpDate(isYearChange: boolean) {
    this.paymentForm
      .get("expDate")
      .setValue(this.monthValue + "/" + this.yearValue);

    if (this.currentYear === this.yearValue && isYearChange) {
      this.monthOptions = this.currentYearMothOptions;
    } else {
      this.monthOptions = this.allMonthOptions;
    }
  }

  ngOnInit() {
    this.auth.currentUser.subscribe(user => {
      this.user = user;

      if (user === null) {
        this.chargeFromBalance.setValue(false);
      }
    });

    this.intReg = INTEGER_REGEXP;
    const currentMonth = new Date().getMonth() + 1;

    for (let i = +currentMonth; i <= 12; i++) {
      this.currentYearMothOptions.push(i);
    }
    for (let i = +this.currentYear; i <= +this.currentYear + 15; i++) {
      this.yearOptions.push(i);
    }

    this.monthOptions = this.currentYearMothOptions;

    this.paymentForm = this.builder.group({
      fName: ["", Validators.required],
      lName: ["", Validators.required],
      ID: [""],
      ccNumber: ["", Validators.required],
      cvv: ["", Validators.required],
      expDate: [currentMonth + "/" + this.currentYear]
    });

    PaymentAutofill.pipe(
      take(1),
      filter(data => data !== null)
    ).subscribe(form => {
      this.paymentForm.patchValue(form);
    });

    this.chargeFromBalance.valueChanges.subscribe(bool => {
      if (bool) {
        this.paymentForm.disable();
      } else {
        this.paymentForm.enable();
      }
    });
  }
}
