import { environment } from 'environments/environment';
import { NotificationService } from '../../../services/notification.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LocalStorageService, ConverterService, BackendService, GoogleAutocompleteService } from '@app/services';
import { CompareRequest, CompareResponseData, TimeRangeResponseData, Locations, ShipmentForm, GooglePlace } from '@app/interfaces';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { RowInsertion } from '@app/animations';


import {
  ORDER_DATA_TOKEN,
  LOCATIONS_TOKEN,
  SHIPMENT_DATA_TOKEN,
  SELECTED_PROCESSOR_DATA_TOKEN,
  NORMALIZED_ORDER_DATA_TOKEN,
  FLOAT_REGEXP
} from '@app/constants';

@Component({
  selector: 'app-pickup-details',
  templateUrl: './pickup-details.component.html',
  styleUrls: ['./pickup-details.component.scss', '../detail-page-root.scss'],
  animations: [RowInsertion]
})

export class PickupDetailsComponent implements OnInit {

  orgAddressVisible = false;
  destAddressVisible = false;
  consigneeCode: string;
  shipperCode: string;

  floatReg: RegExp;
  requesting = false;
  submited: boolean;
  isiFrameOpened: boolean;
  order: CompareRequest;
  normalizedOrder: CompareRequest;
  selectedService: CompareResponseData;
  pickupForm: FormGroup;
  locations: Locations;
  pickupHourOptions: any[];
  timeRangeOptions: TimeRangeResponseData[] = [];
  isNextStep = false;
  isSubmitFailed = false;
  today = new Date();
  isTermsChecked = new FormControl(false);
  isChecked = new FormControl(false);

  shipperAddress = new FormControl('');
  consigneeAddress = new FormControl('');
  unfoundFragments: string[] = [];

  constructor(
    private googleAuto: GoogleAutocompleteService,
    private builder: FormBuilder,
    private store: LocalStorageService,
    private converter: ConverterService,
    private backend: BackendService,
    private router: Router,
    private notification: NotificationService) {}

  prevent(event) {
    event.preventDefault();
  }

  get shipperData(): FormControl {
    return this.pickupForm.get('shipperData') as FormControl;
  }

  get consigneeData(): FormControl {
    return this.pickupForm.get('consigneeData') as FormControl;
  }

  get shipperCountry(): FormControl {
    return this.shipperData.get('country') as FormControl;
  }

  get consigneeCountry(): FormControl {
    return this.consigneeData.get('country') as FormControl;
  }

  get contactsValid(): boolean {
    return this.consigneeData.valid && this.shipperData.valid;
  }

  showError(msg: string, duration: number = 5) {
    this.notification.errorNotification(msg);
  }

  onSubmit() {

    if (this.isNextStep) {
      if (!this.isTermsChecked.value || !this.isChecked.value) {
        this.isSubmitFailed = true;
        return;
      }

      if (this.pickupForm.valid) {
        this.createShipment();
      } else {
        this.isSubmitFailed = true;
        this.showError('the form is not valid');
      }
    } else {

      if (this.contactsValid === false) {
        this.isNextStep = false;
        this.isSubmitFailed = true;
        this.showError('The form is not valid');
      } else {
        this.isNextStep = true;
        this.isSubmitFailed = false;
      }
    }

  }

  async createShipment() {

    this.requesting = true;

    try {
      this.store.set<ShipmentForm>(SHIPMENT_DATA_TOKEN, this.pickupForm.value);

      const payload = this.pickupForm.value;
      payload.shipperData.phone = this.shipperCode + '-' + payload.shipperData.phone;
      payload.consigneeData.phone = this.consigneeCode + '-' + payload.consigneeData.phone;

      const request = await this.converter.convertToShipmentRequest(payload);
      const response = await this.backend.executeShipment(request);
      const data = response.data[0][0];
      this.router.navigate(['/ship', 'payment_details', data.pickupId]);
    } catch (error) {
      this.showError(error, 10);
    } finally {
      this.requesting = false;
    }
  }



  async ngOnInit() {

    this.floatReg = FLOAT_REGEXP;

    if (!environment.production) {
      this.isChecked.setValue(true);
      this.isTermsChecked.setValue(true);
    }

    this.order = this.store.get<CompareRequest>(ORDER_DATA_TOKEN);
    this.locations = this.store.get<Locations>(LOCATIONS_TOKEN);
    this.selectedService = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);

    this.pickupForm  = this.builder.group({

      documentDescription: ['', Validators.required],
      Vat_TaxId : [''],
      estimatePackageValue: [''],
      currency: ['USD'],
      reason: ['', Validators.required],
      receiveInvoice: 'sender',
      payTaxes: 'receiver',

      shipperData: this.builder.group({
        name : ['', Validators.required],
        country: ['', Validators.required],
        state : [''],
        city: ['', Validators.required],
        street : ['', Validators.required],
        houseNumber : [''],
        postalCode : ['', Validators.required],
        contactPerson : ['', Validators.required],
        phone : ['', [Validators.required, Validators.minLength(8)]],
        email : ['', [Validators.required, Validators.email]],
        additionalInstructions: [''],
      }),

      consigneeData: this.builder.group({
        name : ['', Validators.required],
        country: ['', Validators.required],
        state : [''],
        city: ['', Validators.required],
        street : ['', Validators.required],
        houseNumber : [''],
        postalCode : ['', Validators.required],
        contactPerson : ['', Validators.required],
        phone : ['', [Validators.required, Validators.minLength(8)]],
        email : ['', [Validators.required, Validators.email]],
        additionalInstructions: [''],
      }),

      estimatedDeliveryDate: this.builder.group({
        pickupDate: [''],
        pickupTime: [''],
      })

    });





    this.normalizedOrder = this.converter.normalizeOrder(this.order, this.selectedService);
    const hasChanged = this.store.compare<CompareRequest>(NORMALIZED_ORDER_DATA_TOKEN, this.normalizedOrder);
    this.pickupForm.patchValue(this.normalizedOrder);


    this.pickupForm.get('estimatedDeliveryDate').get('pickupDate').valueChanges.subscribe(val => {
      this.timeRangeOptions = [];
      this.backend.executeGetProcessorTimeRange(this.converter.getTimeRangeRequest(val))
      .then(response => {
        this.timeRangeOptions = response.data[0];
        this.pickupForm.get('estimatedDeliveryDate').get('pickupTime')
        .setValue(response.data[0][0].startTime);
      });
    });


    this.pickupForm.get('estimatedDeliveryDate').get('pickupDate')
    .setValue(
      moment(this.selectedService.pickupDate).isValid() ?
      moment(this.selectedService.pickupDate, 'YYYY-MM-DD').toDate() :
      moment().add(1, 'day').toDate()
    );

    const shipmentDetails = this.store.get<ShipmentForm>(SHIPMENT_DATA_TOKEN);
    if (hasChanged && shipmentDetails) {
      this.pickupForm.patchValue(shipmentDetails);
      this.isNextStep = true;
    }


    let org = '';
    let dest = '';

    ['city', 'state', 'country'].forEach(key => {

      if (this.shipperData.get(key).value) {
        org += `${this.shipperData.get(key).value}, `;
      }
      if (this.consigneeData.get(key).value) {
        dest += `${this.consigneeData.get(key).value}, `;
      }
    });

    this.shipperAddress.setValue(org);
    this.consigneeAddress.setValue(dest);


    this.shipperAddress.valueChanges.subscribe(value => {

      if (value.indexOf(org) === 0) {
      } else {
        if (org.indexOf(value) >= 0) {
          this.shipperAddress.setValue(org);
        } else {
          this.shipperAddress.setValue(org + value);
        }
      }
    });

    this.consigneeAddress.valueChanges.subscribe(value => {
      if (value.indexOf(dest) === 0) {
      } else {
        if (dest.indexOf(value) >= 0) {
          this.consigneeAddress.setValue(dest);
        } else {
          this.consigneeAddress.setValue(dest + value);
        }
      }
    });

  }



  async setAddress(place: GooglePlace, target: 'shipper' | 'consignee'): Promise<void> {
    const street = this.googleAuto.getStreet(place);
    const houseNumber = this.googleAuto.getHouseNumber(place);
    const postalCode = await this.googleAuto.retrievePostalCode(place);


    if (street) {
      this[target + 'Data'].get('street').setValue(street);
    } else if (!this[target + 'Data'].get('street').value) {
      this.unfoundFragments.push(target + 'Street');
    }

    if (houseNumber) {
      this[target + 'Data'].get('houseNumber').setValue(houseNumber);
    } else if (!this[target + 'Data'].get('houseNumber').value) {
      this.unfoundFragments.push(target + 'House');
    }

    if (postalCode) {
      this[target + 'Data'].get('postalCode').setValue(postalCode);
    } else if (!this[target + 'Data'].get('postalCode').value) {
      this.unfoundFragments.push(target + 'PostalCode');
    }

    setTimeout(() => {
      this.unfoundFragments = [];
    }, 2000);


  }

}


