import { Component, OnInit } from '@angular/core';
import { BackendService, LocalStorageService } from '@app/services';
import { CompareResponseData, CompareRequest, CompareResponseAdditional } from '@app/interfaces';
import { ORDER_DATA_TOKEN } from '@app/constants';

@Component({
  selector: 'app-service-selection',
  templateUrl: './service-selection.component.html',
  styleUrls: ['./service-selection.component.scss']
})
export class ServiceSelectionComponent implements OnInit {

  options: Promise<CompareResponseData[]>;
  constructor(
    private backend: BackendService,
    private store: LocalStorageService) { }

  ngOnInit() {
    const response = this.backend.executeCompare(this.store.get<CompareRequest>(ORDER_DATA_TOKEN));

    this.options = response.then(({data, additionalInfo}) => {

      this.store.set<CompareResponseAdditional>('__PACKAGE__DETAILS__', additionalInfo[0]);
      return data.filter(option => option.errMsg === 'null').sort((a, b) => a.totalPrice_USD - b.totalPrice_USD);
    });
  }

}
