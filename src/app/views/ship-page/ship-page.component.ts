import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  CompareResponseData,
  CompareResponseAdditional,
  Locations,
  CompareRequest,
  ShipmentForm,
  ShipmentRequest,
  User
} from '@app/interfaces';
import { BackendService, LocalStorageService, ConverterService, AuthService } from '@app/services';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, startWith } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ORDER_DATA_TOKEN, LOCATIONS_TOKEN, SHIPMENT_DATA_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN } from '@app/constants';
import { CurrencyObserver } from '@app/utils/observers';

@Component({
  selector: 'app-ship-page',
  templateUrl: './ship-page.component.html',
  styleUrls: ['./ship-page.component.scss']
})
export class ShipPageComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  user: User;
  currentStepIndex: number;
  locations: Locations;
  order: CompareRequest;
  selectedService: CompareResponseData;
  shipmentDetails: ShipmentRequest;
  currencyObserver = CurrencyObserver;

  packages: CompareResponseAdditional[] = [];
  shippingTypeId: number;



  constructor(
    private auth: AuthService,
    private backend: BackendService,
    private store: LocalStorageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private converter: ConverterService) {}

  setCurrentStepIndex() {
    this.currentStepIndex = this.activatedRoute.snapshot.firstChild.data.step;
    this.order = this.store.get<CompareRequest>(ORDER_DATA_TOKEN);
    this.selectedService = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);
    const shipmentData = this.store.get<ShipmentForm>(SHIPMENT_DATA_TOKEN);
    this.shipmentDetails = shipmentData && this.converter.convertToShipmentRequest(shipmentData);
  }

  ngOnInit() {
    this.locations = this.store.get<Locations>(LOCATIONS_TOKEN);
    this.subscription = new Subscription();

    this.subscription.add(
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        startWith(null)
      ).subscribe(() => this.setCurrentStepIndex())
    );

    this.subscription.add(
      this.auth.currentUser.subscribe(user => {
        this.user = user;
      })
    );

    this.backend.executeCompare(this.order)
    .then(({data, additionalInfo}) => {
      this.packages = additionalInfo.map(info => {
        const weightToCharge = parseFloat(info.weightToCharge.toFixed(2));
        return {...info, weightToCharge};
      });

      const sample = data.find(item => item.errMsg === 'null');
      this.shippingTypeId = sample && sample.shippingTypeId;

      // for api bug
      if (typeof this.shippingTypeId !== 'number') {
        this.shippingTypeId = 2;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
