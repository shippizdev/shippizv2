import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '@app/services';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.scss']
})
export class EmailVerificationComponent implements OnInit {

  constructor(private activeRoute: ActivatedRoute, private backend: BackendService) { }

  successMsg: string;
  errMsg: string;

  ngOnInit() {
    const map = this.activeRoute.snapshot.queryParamMap;
    const email = map.get('email');
    const userId = map.get('user');
    const token = '';
    const ip = '';

    this.backend.execute('EmailConfirmation', {email, userId, token, ip}).then(res => {
      this.successMsg = res.data[0][0].instructions;
    }).catch(err => {
      this.errMsg = err;
    })
  }

}
