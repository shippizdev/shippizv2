import { LocalStorageService } from './../../../services/local-storage.service';
import { CompareResponseData } from './../../../interfaces/responses/compare-response';
import { BackendService } from './../../../services/backend.service';
import { ConverterService } from './../../../services/converter.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import {SELECTED_PROCESSOR_DATA_TOKEN} from '@app/constants'
@Component({
  selector: 'app-business-excel',
  templateUrl: './business-excel.component.html',
  styleUrls: ['./business-excel.component.scss']
})
export class BusinessExcelComponent implements OnInit {
  pickupForm;
  timeRangeOptions;
  selectedService: CompareResponseData;

  constructor(
    private builder: FormBuilder,
    private converter: ConverterService,
    private backend: BackendService,
    private store: LocalStorageService) {}

  ngOnInit() {
    // this.pickupForm  = this.builder.group({
    //   estimatedDeliveryDate: this.builder.group({
    //     pickupDate: [''],
    //     pickupTime: [''],
    //   })

    // });

    // this.selectedService = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);

    // this.pickupForm.get('estimatedDeliveryDate').get('pickupDate').valueChanges.subscribe(val => {
    //   this.timeRangeOptions = [];
    //   this.backend.executeGetProcessorTimeRange(this.converter.getTimeRangeRequest(val))
    //   .then(response => {
    //     this.timeRangeOptions = response.data[0];
    //     this.pickupForm.get('estimatedDeliveryDate').get('pickupTime')
    //     .setValue(response.data[0][0].startTime);
    //   });
    // });

    // this.pickupForm.get('estimatedDeliveryDate').get('pickupDate')
    // .setValue(moment(this.selectedService.pickupDate, 'YYYY-MM-DD').toDate());

  }

}
