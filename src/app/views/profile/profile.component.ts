import { Component, OnInit } from '@angular/core';
import { User } from '@app/interfaces';
import { AuthService } from '@app/services';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  currentUser: Observable<User>;
  navbarVisible = false;

  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.currentUser = this.auth.currentUser;
  }

}
