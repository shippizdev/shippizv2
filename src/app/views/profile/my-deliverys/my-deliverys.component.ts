import { Component, OnInit } from '@angular/core';
import { UserOrderData } from '@app/interfaces/responses/user-orders';
import { BackendService, AuthService } from '@app/services';
import { simpleParse } from '@app/utils/functions';
import { switchMap, filter} from 'rxjs/operators';

@Component({
  selector: 'app-my-deliverys',
  templateUrl: './my-deliverys.component.html',
  styleUrls: ['./my-deliverys.component.scss']
})
export class MyDeliverysComponent implements OnInit {

  orders: UserOrderData[] = [];
  paginatedOrders: UserOrderData[] = [];
  resolved = false;

  constructor(private backend: BackendService, private auth: AuthService) { }

  handlePaginator(event) {
    const {previousPageIndex, pageIndex, pageSize, length} = event;
    const offset = pageIndex * pageSize;
    this.paginatedOrders = this.orders.slice(offset, offset + pageSize);
  }

  ngOnInit() {

    this.auth.currentUser.pipe(
      filter(user => !!user),
      switchMap(user => {
        const { sessionId} = user;
        return this.backend.execute('GetUserOrders', {sessionId}).then(({data}) => {
          return data[0].map(dt => simpleParse(dt, 'shipperData', 'consigneeData')).filter(d => d.shipperData && d.consigneeData);
        });
      })
    ).subscribe(res => {
      this.orders = res;
      this.resolved = true;
      this.paginatedOrders = this.orders.slice(0, 5);
    });

  }

}
