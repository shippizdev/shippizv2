import { ConverterService, BackendService, LocalStorageService } from '@app/services';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';

import { RowInsertion } from '@app/animations';
import { INTEGER_REGEXP, PICKUP_DATA_TOKEN } from '@app/constants';
@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss'],
  animations: [RowInsertion]

})
export class CreditCardComponent implements OnInit {


  intReg: RegExp;
  isSubmitted = false;
  monthOptions: number[] = [];
  allMonthOptions: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  currentYearMothOptions: number[] = [];
  yearOptions: number[] = [];
  currentYear = new Date().getFullYear().toString().slice(-2);
  errorMes: string;


  paymentForm: FormGroup;
  constructor(
    private builder: FormBuilder,
    private activeRoute: ActivatedRoute,
    private converter: ConverterService,
    private backend: BackendService,
    private store: LocalStorageService,
    private router: Router,
   ) { }


  @ViewChildren('expDate')
  expDateControls: QueryList<ElementRef<HTMLSelectElement>>;

  get yearValue() {
    return this.expDateControls.last.nativeElement.value;
  }

  get monthValue() {
    return this.expDateControls.first.nativeElement.value;
  }

  setExpDate(isYearChange: boolean) {
    this.paymentForm.get('expDate')
    .setValue(this.monthValue + '/' + this.yearValue);

    if (this.currentYear === this.yearValue && isYearChange) {
      this.monthOptions = this.currentYearMothOptions;
    } else {
      this.monthOptions = this.allMonthOptions;
    }

  }



  ngOnInit() {

    this.intReg = INTEGER_REGEXP;
    const currentMonth =  new Date().getMonth() + 1;

    for (let i = +currentMonth; i <= 12; i++) {
      this.currentYearMothOptions.push(i);
    }
    for (let i = +this.currentYear; i <= +this.currentYear + 15; i++) {
      this.yearOptions.push(i);
    }

    this.monthOptions = this.currentYearMothOptions;

    this.paymentForm = this.builder.group({

      fName: ['', Validators.required],
      lName: ['', Validators.required],
      ID: ['', Validators.required],
      ccNumber: ['', Validators.required],
      cvv: ['', Validators.required],
      expDate: [currentMonth + '/' + this.currentYear],

    });

}
}
