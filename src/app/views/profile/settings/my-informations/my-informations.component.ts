import { NotificationService } from '../../../../services/notification.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { RowInsertion } from '@app/animations';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService, BackendService, ConverterService } from '@app/services';
import { AutocompleteComboBoxComponent } from '@app/components/autocomplete-combo-box/autocomplete-combo-box.component';
import { delay } from 'rxjs/operators';
import moment from 'moment';
import { passwordConfirm } from '@app/utils/validators';
import { User } from '@app/interfaces';

@Component({
  selector: 'app-my-informations',
  templateUrl: './my-informations.component.html',
  styleUrls: ['./my-informations.component.scss'],
  animations: [RowInsertion],
})
export class MyInformationsComponent implements OnInit, AfterViewInit {

  userDetailsForm: FormGroup;
  changePasswordForm: FormGroup;
  changePasswordFormVisible = false;
  user: User;

  @ViewChild(AutocompleteComboBoxComponent, {static: false})
  autocompleteRef: AutocompleteComboBoxComponent;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private backend: BackendService,
    private converter: ConverterService,
    private notification: NotificationService) { }

  updateUser() {
    const payload = this.converter.convertToUpdateUserDetailsRequest(this.userDetailsForm.value);
    this.backend.execute('UpdateUserDetails', payload).then(({data}) => {
      this.auth.currentUser.next(data[0][0]);
      this.notification.successNotification('User was updated successfully')
    });
  }

  updatePassword() {
    const {oldPassword, password} = this.changePasswordForm.value;

    const {email, sessionId} = this.user;
    this.backend.execute('UpdatePassword', {
      email,
      sessionId,
      oldPassword,
      password,
    }).then(({data}) => {
      this.notification.successNotification(data[0][0].instruction)
      this.changePasswordForm.reset();
      this.changePasswordFormVisible = false;
    }).catch(err => {
      this.notification.errorNotification(err)
    });
  }

  get country(): FormControl {
    return this.userDetailsForm.get('country') as FormControl;
  }

  get city(): FormControl {
    return this.userDetailsForm.get('city') as FormControl;
  }



  ngOnInit() {

    this.userDetailsForm = this.fb.group({
      email: [''],
      fName: [''],
      lName: [''],
      isMale: [true],
      phone: [''],
      zip: [''],
      birthDate: [''],
      company: [''],
      country: [''],
      city: [''],
      address: [''],
      profession: ['']
    });

    this.changePasswordForm = this.fb.group({
      oldPassword: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    }, {validators: passwordConfirm});


  }

  ngAfterViewInit() {

    this.auth.currentUser.pipe(delay(0)).subscribe(user => {
      if (user) {
        this.user = user;
        this.userDetailsForm.patchValue(Object.assign({}, user, {birthDate: moment(user.birthDate, 'YYYY/MM/DD').format('YYYY-MM-DD')}));
        this.autocompleteRef.remoteSet({country: user.country, city: user.city});
      }
    });
  }


}
