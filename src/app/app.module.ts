import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { PopoverModule } from 'ngx-bootstrap/popover';

import {
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatExpansionModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSnackBarModule,
  MatButtonModule,
  MatAutocompleteModule,
  MatDialogModule,
  MatMenuModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatPaginatorIntl

} from '@angular/material';

import { NgtUniversalModule } from '@ng-toolkit/universal';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { NguCarouselModule } from '@ngu/carousel';
import { DevelopmentModule } from '@app/development';
import { TranslatorModule } from '@app/translator';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';

import {
  HeaderComponent,
  FooterComponent,
  HomePageComponent,
  ShipPageComponent,
  ServiceSelectionComponent,
  PickupDetailsComponent,
  PaymentDetailsComponent,
  ConfirmationPageComponent,
  ProfileComponent,
  MyDeliverysComponent,
  BusinessExcelComponent,
  MyContactsComponent,
  MyStoreComponent,
  SettingsComponent,
  MyInformationsComponent,
  CreditCardComponent,
  OptionsComponent,
} from '@app/views';


import {
  OptionCardComponent,
  DeliveryReportComponent,
  ForgotPasswordComponent,
  CountryAutocompleteComponent,
  AutocompleteComboBoxComponent,
  AdvantageRowComponent,
  ResetPasswordComponent,
  InvoiceComponent,
  LoadingAnimationComponent,
  CardComponent,
  ExpandableContentComponent,
  ShippizPopupComponent,
} from '@app/components';


import {
  GoogleAutocompleteDirective,
  RegexpRestrictionDirective,
  FloatingLabelDirective,
  BlurEventDirective,
  StopPropogetionEventDirective,
} from '@app/directives';


import {
  CapitalPipe,
  PackageTypePipe,
  SafeDatePipe,
  CountryNamePipe,
  ProcessorLogoPipe,
  RelevantPricePipe,
  CurrencySignPipe,
  HighlightPipe
} from '@app/pipes';

import {
  NotFoundPageComponent,
  ServicesComponent,
  FAQComponent,
  BusinessComponent,
  AboutUsComponent,
  ContactUsComponent,
  AboutShippizComponent,
  BeforeOrderingComponent,
  AfterOrderingComponent,
  AboutUsContentComponent,
  UpsWarningComponent,
  WelcomePopupComponent,
} from '@app/static-templates';
import { EmailVerificationComponent } from './views/email-verification/email-verification.component';
import { InvoicePageComponent } from './static-templates/invoice-page/invoice-page.component';
import { HeadersInterceptor, ParamsInterceptor } from './classes';
import { PhoneAutocompleteComponent } from './components/phone-autocomplete/phone-autocomplete.component';
import { ExternalPickupFormComponent } from './components/external-pickup-form/external-pickup-form.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { ExternalPickupFormHebrewComponent } from './components/external-pickup-form hebrew/external-pickup-form-hebrew.component';
import { DomesticPickupILComponent } from './views/ship-page/domestic-pickup-il/domestic-pickup-il.component';
import { YdmPopupComponent } from './static-templates/ydm-popup/ydm-popup.component';


export class CustomPaginator extends MatPaginatorIntl {
  constructor() {
    super();
    this.itemsPerPageLabel = 'size';
  }
}

@NgModule({

  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundPageComponent,
    OptionCardComponent,
    GoogleAutocompleteDirective,
    RegexpRestrictionDirective,
    CapitalPipe,
    PackageTypePipe,
    SafeDatePipe,
    CountryNamePipe,
    ShipPageComponent,
    ServiceSelectionComponent,
    PickupDetailsComponent,
    PaymentDetailsComponent,
    ProcessorLogoPipe,
    ConfirmationPageComponent,
    ProfileComponent,
    MyDeliverysComponent,
    BusinessExcelComponent,
    MyContactsComponent,
    MyStoreComponent,
    SettingsComponent,
    DeliveryReportComponent,
    ServicesComponent,
    FAQComponent,
    BusinessComponent,
    AboutUsComponent,
    ContactUsComponent,
    MyInformationsComponent,
    CreditCardComponent,
    OptionsComponent,
    AboutShippizComponent,
    BeforeOrderingComponent,
    AfterOrderingComponent,
    ForgotPasswordComponent,
    CountryAutocompleteComponent,
    AutocompleteComboBoxComponent,
    HighlightPipe,
    AdvantageRowComponent,
    ResetPasswordComponent,
    AboutUsContentComponent,
    InvoiceComponent,
    LoadingAnimationComponent,
    CardComponent,
    ExpandableContentComponent,
    FloatingLabelDirective,
    BlurEventDirective,
    StopPropogetionEventDirective,
    UpsWarningComponent,
    WelcomePopupComponent,
    RelevantPricePipe,
    CurrencySignPipe,
    ShippizPopupComponent,
    EmailVerificationComponent,
    InvoicePageComponent,
    PhoneAutocompleteComponent,
    ExternalPickupFormComponent,
    ExternalPickupFormHebrewComponent,
    ConfirmationComponent,
    DomesticPickupILComponent,
    YdmPopupComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    TranslatorModule,
    DevelopmentModule,
    CommonModule,
    TransferHttpCacheModule,
    NgtUniversalModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    NguCarouselModule,
    MatIconModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatMenuModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    PopoverModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBxrVr68fp438DqsrrypC457xwcBC4XxiM'
    })
  ],

  entryComponents: [ResetPasswordComponent, InvoiceComponent, UpsWarningComponent, WelcomePopupComponent, ConfirmationComponent, YdmPopupComponent],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeadersInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamsInterceptor,
      multi: true,
    },
    {
      provide: MatPaginatorIntl,
      useClass: CustomPaginator
    }
  ],
  bootstrap: [AppComponent],

})

export class AppModule { }
