import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LanguageObserver } from '@app/utils/observers';
import { UID_TOKEN } from '@app/constants';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const sessionId = JSON.parse(localStorage.getItem(UID_TOKEN)) || 'null';
        const lang = LanguageObserver.value.code.toUpperCase();
        const brandId = '0';
        const ip = '0';



        return next.handle(req.clone({
            headers: new HttpHeaders({sessionId, lang, brandId, ip})
        }));
    }
}
