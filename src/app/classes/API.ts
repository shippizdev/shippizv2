import ApiEndpoint from '@assets/api-endpoint.json';

export class API {

  public static __enviorment = 'dev';

  private static get jsonFile() {
    const {local, dev, prod} = ApiEndpoint;
    switch (this.__enviorment) {
      case 'local': return local;
      case 'dev': return dev;
      case 'prod': return prod;
    }
  }

  private static _excecuteURL: string = null;
  private static _excecuteCompareURL: string = null;
  private static _executeGetProcessorTimeRangeURL: string = null;
  private static _executeShipmentURL: string = null;
  private static _excecutePickupURL: string = null;
  private static _excecuteDeletePickupURL: string = null;

  public static setEnvironment(env: 'dev' | 'local' | 'prod') {
    Object.keys(API).filter(key => key.startsWith('_')).forEach(key => API[key] = null)
    API.__enviorment = env;
  }


  public static get excecuteURL() {
    if (this._excecuteURL == null) {
      this._excecuteURL = this.jsonFile.excecuteURL;
    }
    return this._excecuteURL;
  }

  public static get excecuteCompareURL() {
    if (this._excecuteCompareURL == null) {
      this._excecuteCompareURL = this.jsonFile.excecuteCompareURL;
    }
    return this._excecuteCompareURL;
  }

  public static get executeGetProcessorTimeRangeURL() {
    if (this._executeGetProcessorTimeRangeURL == null) {
      this._executeGetProcessorTimeRangeURL = this.jsonFile.executeGetProcessorTimeRangeURL;
    }
    return this._executeGetProcessorTimeRangeURL;
  }

  public static get executeShipmentURL() {
    if (this._executeShipmentURL == null) {
      this._executeShipmentURL = this.jsonFile.executeShipmentURL;
    }
    return this._executeShipmentURL;
  }

  public static get excecutePickupURL() {
    if (this._excecutePickupURL == null) {
      this._excecutePickupURL = this.jsonFile.excecutePickupURL;
    }
    return this._excecutePickupURL;
  }

  public static get excecuteDeletePickupURL() {
    if (this._excecuteDeletePickupURL == null) {
      this._excecuteDeletePickupURL = this.jsonFile.excecuteDeletePickupURL;
    }
    return this._excecuteDeletePickupURL;
  }

}
