import { Injectable } from '@angular/core';
import { GooglePlace, GoogleGeocoderResult } from '@app/interfaces';
import { combine } from '@app/utils/functions';


interface Coordinates {
  lat: number;
  lng: number;
}


const OK = google.maps.GeocoderStatus.OK;


@Injectable({
  providedIn: 'root'
})

export class GoogleAutocompleteService {

  private geocoder = new google.maps.Geocoder();

  constructor() {}

  public async retrievePostalCode(place: GooglePlace) {

    const defaultPostalCode = this.getDefaultPostalCode(place);
    
    if (defaultPostalCode) {
      return defaultPostalCode;
    }

    const location = this.getCoordinates(place);
    const places = await this.getPlaceByCoords(location);
    const postalCode = this.getPostalCode(places);
    return postalCode;
  }

  public getCountry(place: GooglePlace): string {
    return (
      this.retrieveArea(place, ['country'], true) ||
      this.retrieveAreaFromMarkup(place, '.country-name')
    );
  }

  public getCity(place: GooglePlace): string {
    return (
      this.retrieveArea(place, ['locality', 'administrative_area_level_3', 'postal_town']) ||
      this.retrieveAreaFromMarkup(place, '.locality')
    );
  }

  public getStreet(place: GooglePlace): string {
    return this.retrieveArea(place, ['route']);
  }

  public getHouseNumber(place: GooglePlace): string {
    return this.retrieveArea(place, ['street_number']);
  }

  public getState(place: GooglePlace) {
    return this.retrieveAreaFromMarkup(place, '.region');
  }


  public retrieveAreas(place: GooglePlace) {
    const country = this.getCountry(place);
    const city = this.getCity(place);
    const state = this.getState(place);
    const street = this.getStreet(place);
    const houseNumber = this.getHouseNumber(place);

    return {country, city, state, street, houseNumber};
  }





  private getCoordinates(place: GooglePlace): Coordinates {
    return {
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    };
  }

  private getPlaceByCoords(location: Coordinates): Promise<GoogleGeocoderResult[]> {

    return new Promise((resolve, reject) => {
      this.geocoder.geocode({location}, (results, status) => {
        if (status === OK) {
          resolve(results);
        } else {
          reject(status);
        }
      });
    });
  }

  private getPostalCode(places: GoogleGeocoderResult[]): string {

    const placeWithPostalCode = places.find(place => {
      return place.address_components.some(address => {
        return address.types.includes('postal_code');
      });
    });

    if (placeWithPostalCode) {
      const options = placeWithPostalCode.address_components;
      return combine(options[options.length - 1].long_name);
    }

    return '';
  }

  private getDefaultPostalCode(place: GooglePlace): string {
    const defaultPostalCode = place.address_components.find(address => {
      return address.types.includes('postal_code');
    });

    if (defaultPostalCode) {
      return combine(defaultPostalCode.long_name);
    }

  }

  private retrieveArea(place: GooglePlace, types: string[], shortName: boolean = false): string {
    const area = place.address_components.find(address => {
      return address.types.some(type => {
        return types.includes(type);
      });
    });

    return area ? area[shortName ? 'short_name' : 'long_name'] : '';
  }

  private retrieveAreaFromMarkup(place: GooglePlace, selector: string): string {
    const markup = place.adr_address;
    const template = document.createElement('span');
    template.insertAdjacentHTML('afterbegin', markup);

    const targetFragment = template.querySelector(selector);
    return targetFragment ? targetFragment.textContent : '';
  }


}
