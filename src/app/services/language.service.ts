import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class LanguageService {
  constructor() {}
  public langtype$ = new BehaviorSubject<string>("en");
  langtype: Observable<string> = this.langtype$.asObservable();
  setLanguage(type) {
    this.langtype$.next(type);
  }
}
