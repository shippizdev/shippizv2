import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { BackendService } from './backend.service';
import { simpleParse } from '@app/utils/functions';
import { InvoiceComponent } from '@app/components';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class InvoiceManagerService {

  constructor(private backend: BackendService, private auth: AuthService, private dialog: MatDialog) { }

  async openInvoice(pickupId: number, extraOptions: MatDialogConfig = {}) {

    let content = {};
    if (pickupId) {
      const user = this.auth.currentUser.value
      const sessionId = user ? user.sessionId : null;
      const {data} = await this.backend.execute('GetInvoice', {pickupId, sessionId});
      content = simpleParse(data[0][0], 'shipperData', 'consigneeData', 'invoice');
    }

    this.dialog.open(InvoiceComponent, {
      width: '900px',
      autoFocus: false,
      disableClose: true,
      panelClass: 'custom-dialog-class',
      data: content,
      ...extraOptions,
    });
  }
}
