import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '@app/interfaces';
import { LocalStorageService } from './local-storage.service';
import { BackendService } from './backend.service';
import { Router } from '@angular/router';
import { UID_TOKEN } from '@app/constants';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  public readonly currentUser = new BehaviorSubject<User>(null);

  constructor(
    private store: LocalStorageService,
    private backend: BackendService,
    private router: Router) {

    setTimeout(() => {
      this.syncUser();
    });

  }

  public async login({email, password}): Promise<void> {

    const payload = {email, password};
    const {data} = await this.backend.execute('Login', payload);
    const user = data[0][0];
    this.store.set<string>(UID_TOKEN, user.sessionId, true);
    this.currentUser.next(user);

    if (user.isUpdateDetails === false) {
      this.router.navigate(['/profile/settings']);
    }

  }

  public async logout() {

    const {sessionId} = this.currentUser.value;
    const payload = {sessionId};

    await this.backend.execute('LogOut', payload);

    this.store.remove(UID_TOKEN, true);
    this.currentUser.next(null);

    if (this.router.url.includes('profile')) {
      this.router.navigate(['home']);
    }

  }

  public async register({email, password}): Promise<void> {

    const payload = {email, password};
    const {data} = await this.backend.execute('RegisterUser', payload);
    const user = data[0][0];
    this.store.set<string>(UID_TOKEN, user.sessionId, true);

    this.currentUser.next(user);
    this.router.navigate(['/profile/settings']);

  }

  public async syncUser() {

    const sessionId = this.store.get<string>(UID_TOKEN, true);
    if (sessionId === null) {
      return;
    }

    const payload = {sessionId};

    try {
      const {data} = await this.backend.execute('GetUserDetails', payload);
      this.currentUser.next(data[0][0]);
    } catch (err) {
      this.currentUser.next(null);
      this.store.remove(UID_TOKEN, true);
      if (this.router.url.includes('profile')) {
        this.router.navigate(['home']);
      }
    }
  }
}
