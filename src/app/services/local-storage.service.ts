import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {

  constructor() {}

  get<T>(key: string, global?: boolean): T {
    if (global) {
      return JSON.parse(localStorage.getItem(key));
    }

    return JSON.parse(sessionStorage.getItem(key));
  }

  set<T>(key: string, payload: T, global?: boolean): void {
    if (global) {
      localStorage.setItem(key, JSON.stringify(payload));
    }
    sessionStorage.setItem(key, JSON.stringify(payload));
  }

  remove(key: string, global?: boolean): void {
    if (global) {
      localStorage.removeItem(key);
    }
    sessionStorage.removeItem(key);
  }

  removeAll(global?: boolean): void {
    if (global) {
      localStorage.clear();
    }
    sessionStorage.clear();
  }

  compare<T>(key: string, payload: T): boolean {
    if (JSON.stringify(payload) === sessionStorage.getItem(key)) {
      return true;
    }

    sessionStorage.setItem(key, JSON.stringify(payload));
    return false;
  }

}
