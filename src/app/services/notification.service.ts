import { MatSnackBar, MatDialog } from '@angular/material';
import { Injectable } from '@angular/core';
import { ERROR_MESSAGE_DURATION } from '@app/constants';
import { ConfirmationComponent } from '@app/components/confirmation/confirmation.component';
@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  defaultDuration = ERROR_MESSAGE_DURATION;

  constructor(private snackbar: MatSnackBar, private dialog: MatDialog) {}

  openConfirmPopup(msg: string[], fn: () => void) {
    this.dialog.open(ConfirmationComponent, {
      data: {fn, msg}
    });
  }

  errorNotification(message: string, duration?: number) {
    this.snackbar.open(message, 'OK', {
      duration: duration ||  this.defaultDuration,
      panelClass: ['notif-error']
    });
  }

  successNotification(message: string, duration?: number) {
    this.snackbar.open(message, 'OK', {
      duration: duration ||  this.defaultDuration,
      panelClass: ['notif-success']
    });
  }
}
