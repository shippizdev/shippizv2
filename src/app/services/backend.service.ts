import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API } from '@app/classes';
import { CompareResponse, CompareRequest, ShipmentRequest, TimeRangeRequest, TimeRangeResponse } from '@app/interfaces';
import { ShipmentResponse } from '@app/interfaces/responses/shipment-response';
import { PickupRequest } from '@app/interfaces/requests/pickup-request';
import { PickupResponse } from '@app/interfaces/responses/pickup-response';

@Injectable({
  providedIn: 'root'
})

export class BackendService {

  static activeRequests = {};

  constructor(private http: HttpClient) {}

  /* ---------- public API ---------- */

  public executeDeletePickup(payload: any): Promise<any> {
    return this.asPromise(API.excecuteDeletePickupURL, payload);
  }

  public executeGetProcessorTimeRange(payload: TimeRangeRequest): Promise<TimeRangeResponse> {
    return this.asPromise(API.executeGetProcessorTimeRangeURL, payload);
  }

  public executePickup(payload: PickupRequest): Promise<PickupResponse> {
    return this.asPromise(API.excecutePickupURL, payload);
  }

  public executeShipment(payload: ShipmentRequest): Promise<ShipmentResponse> {
    return this.asPromise(API.executeShipmentURL, payload);
  }

  public executeCompare(payload: CompareRequest): Promise<CompareResponse> {
    if (BackendService.activeRequests[payload.action]) {
      return BackendService.activeRequests[payload.action];
    }
    
    const request = this.asPromise(API.excecuteCompareURL, payload);
    BackendService.activeRequests[payload.action] = request;
    return request.then(res => {
      delete BackendService.activeRequests[payload.action];
      return res;
    });
  }

  public execute(action: string, payload: any) {

    return this.asPromise(API.excecuteURL, {action, ...payload});
  }

  /* ---------- Private implementations ---------- */


  private asPromise(apiUrl: string, payload: any, headers?: HttpHeaders): Promise<any> {
    return new Promise((resolve, reject) => {

      this.http.post<any>(apiUrl, payload, {headers})
      .subscribe(response => {
        const {errors} = response;
        if (errors.code) {
          return reject(errors.msg);
        }
        if (errors.Code) {
          return reject(errors.Msg);
        }

        resolve(response);
      });
    });
  }
}
