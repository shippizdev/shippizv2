import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { CACHE_VALIDATION_TIME } from '@app/constants';
import { Processor, Country, PackageType, Currency } from '@app/interfaces';

@Injectable({
  providedIn: 'root'
})

export class CacheService {

  private runningRequests: object = {};
  private ramCache: object = {};

  constructor(private backend: BackendService) {}


  /* ---------- Public API ---------- */

  public clear() {
    this.runningRequests = {};
    this.ramCache = {};
    localStorage.clear();
    sessionStorage.clear();
  }

  public execute(command: any , skipCache: boolean = false): Promise<any> {

    const key = JSON.stringify(command);

    return this.getRunningRequest(key) ||
    this.getFromRamCache(key, skipCache) ||
    this.getFormLocalStorage(key, skipCache) ||
    this.getFromBackend(command, key);

  }



  public getCurrencies(id?: number): Promise<Currency[] | Currency> {
    return this.loadEnums(0, 'id', id);
  }

  public getShippingType(id?: number): Promise<PackageType[] | PackageType> {
    return this.loadEnums(1, 'id', id);
  }

  public getCountries(id?: number): Promise<Country[] | Country> {
    return this.loadEnums(2, 'countryId', id);
  }

  public getProcessorType(id?: number): Promise<Processor[] | Processor> {
    return this.loadEnums(3, 'processorId', id);
  }


  /* ----- Private implementations ----- */


  private now(): number {
    return Math.floor(Date.now() / 1000);
  }

  private isCacheExpired(actionCache: any): boolean {
    const diff = this.now() - actionCache.lastExecuteTime;
    return diff > CACHE_VALIDATION_TIME;
  }

  private getRunningRequest(key: string): Promise<any> {
    if (key in this.runningRequests) {
      return this.runningRequests[key];
    }
  }

  private getFromRamCache(key: string, skip: boolean): Promise<any> {
    if (skip) {
      return;
    }

    if (key in this.ramCache && !this.isCacheExpired(this.ramCache[key])) {
      return this.ramCache[key];
    }
  }

  private getFormLocalStorage(key: string, skip: boolean): Promise<any> {
    if (skip) {
      return;
    }

    if (key in localStorage) {
      const result = JSON.parse(localStorage.getItem(key));
      if (!this.isCacheExpired(result)) {
        return this.ramCache[key] = Promise.resolve(result);
      }
    }
  }

  private getFromBackend(command: any, key: string): Promise<any> {

    const request = new Promise((resolve, reject) => {
      this.backend.execute('', command)
      .then(response => {
        localStorage.setItem(key, JSON.stringify({
          ...response,
          lastExecuteTime: this.now()
        }));

        this.ramCache[key] = Promise.resolve(response);
        resolve(response);
      })
      .catch(err => reject(err))
      .finally(() => delete this.runningRequests[key]);
    });

    return this.runningRequests[key] = request;

  }

  private async loadEnums(entityIndex?: number, idKey?: string, idValue?: number) {
    try {
      const response = await this.execute({ action: 'LoadEnums' });
      if (typeof entityIndex === 'undefined') {
        return response.data;
      } else {
        const items = response.data[entityIndex];
        return idValue ? items.find(item => item[idKey] === idValue) : items;
      }

    } catch (error) {
      console.warn(error);
      return [];
    }
  }

}
