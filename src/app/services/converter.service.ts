import { UserOrderData } from '@app/interfaces/responses/user-orders';
import { Injectable } from '@angular/core';
import { CompareRequest, ShipmentRequest, PaymentForm, DeliveryDateInfo, TimeRangeRequest, CompareResponseData, CompareResponseAdditional, CompareResponse } from '@app/interfaces';
import { ContactInfo, PackageDetail, CompareForm } from '@app/interfaces';
import { ShipmentForm } from '@app/interfaces/form-shapes/shipment-form';
import { LocalStorageService } from './local-storage.service';
import { CacheService } from './cache.service';
import { PickupRequest } from '@app/interfaces/requests/pickup-request';
import moment from 'moment';
import { AuthService } from './auth.service';
import { ORDER_DATA_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN } from '@app/constants';
import { CurrencyHandler } from '@app/utils/observers';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor(
    private store: LocalStorageService,
    private cache: CacheService,
    private auth: AuthService) {}

  public convertToCompareForm(json: CompareRequest): CompareForm {

    const { shipperData: origin, consigneeData: destination, shippingTypeId, packageDetails, weightType, currency } = json;

    const measurementUnits = weightType === 'kg' ? 1 : 2;

    const form = {
      origin,
      destination,
      shippingTypeId,
      packageDetails,
      measurementUnits,
      currency
    };

    return form;
  }

  public convertToCompareRequest(formValue: CompareForm): CompareRequest {

    const { origin, destination, packageDetails, shippingTypeId, measurementUnits, currency } = formValue;

    const [weightType, sizeType] = measurementUnits === 1 ? ['kg', 'cm'] : ['lb', 'inch'];
    const user = this.auth.currentUser.value;

    return {

      action: 'CompareRates',
      sessionId: user ? user.sessionId : null,
      currency,
      shippingTypeId,
      processorId: 0,
      weightType,
      sizeType,
      shipperData: new ContactInfo(origin),
      consigneeData: new ContactInfo(destination),
      packageDetails: packageDetails.map(detail => new PackageDetail(detail)),
      estimatedDeliveryDate: new DeliveryDateInfo()

    };

  }

  public convertToShipmentRequest(formValue: ShipmentForm): ShipmentRequest {


    const selectedProcessorData = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);
    const { shippingTypeId, packageDetails } = this.store.get<CompareRequest>(ORDER_DATA_TOKEN);
    const { processorId } = selectedProcessorData;
    const { weightType, sizeType} = this.store.get<CompareResponseAdditional>('__PACKAGE__DETAILS__');


    const user = this.auth.currentUser.value;
    const currency = CurrencyHandler.value;
    const paymentAmount = selectedProcessorData['totalPrice_' + currency];

    const {
      shipperData,
      consigneeData,
      estimatedDeliveryDate,
      documentDescription = '',
      Vat_TaxId = '',
      estimatePackageValue = '0',
      reason = '',
      receiveInvoice = '',
      payTaxes = '',
    } = formValue;

    return {
      clientJson: {
        userId: user ? user.userId.toString() : null,
        sessionId: user ? user.sessionId : null,
        documentDescription,
        Vat_TaxId,
        estimatePackageValue,
        currency,
        reason,
        processorId,
        paymentAmount: paymentAmount.toString(),
        shippingTypeId,
        weightType,
        sizeType,
        receiveInvoice,
        payTaxes,

        shipperData,
        consigneeData,
        packageDetails: packageDetails.map(detail => new PackageDetail(detail)),
        estimatedDeliveryDate: new DeliveryDateInfo(estimatedDeliveryDate)
      }
    };
  }

  public convertToDeletePickUpRequest(order: UserOrderData) {
    const {processorId, pickupAuthorization, orderNumber: pickupId} = order;
    const user = this.auth.currentUser.value;

    return {
      clientJson: {
        processorId,
        pickupAuthorization,
        pickupId,
        sessionId: user ? user.sessionId : null,
      }
    };
  }

  public convertToUpdateUserDetailsRequest(value: any) {
    const user = this.auth.currentUser.value;
    const { userId, sessionId } = user;

    return Object.assign({}, value, {userId, sessionId});

  }

  public convertToPickupRequest(formValue: PaymentForm, pickupId: string, chargeFromBalance: boolean): PickupRequest {

    const { processorId } = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);
    const user = this.auth.currentUser.value;

    return {
      clientJson: {
        chargeFromBalance,
        userId: user ? user.userId.toString() : null,
        sessionId: user ? user.sessionId : null,
        processorId,
        pickupId: +pickupId,
        billing: formValue,
      }
    };
  }

  public getTimeRangeRequest(date?): TimeRangeRequest {

    const normalizedOrder = this.normalizeOrder(
      this.store.get<CompareRequest>(ORDER_DATA_TOKEN),
      this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN),
    );

    const { shipperData, consigneeData, estimatedDeliveryDate } = normalizedOrder;
    const { processorId } = this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN);

    const pickupDate = date ?
    moment(date).format('YYYY-MM-DD') :
    this.store.get<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN).pickupDate;

    return {
      processorId: parseInt(processorId, 10),

      shipperData: {
        country: shipperData.country,
        city: shipperData.city,
        state: shipperData.state,
        street: shipperData.street,
        houseNumber: shipperData.houseNumber,
        postalCode: shipperData.postalCode,
      },
      consigneeData: {
        country: consigneeData.country,
        city: consigneeData.city,
        state: consigneeData.state,
        street: consigneeData.street,
        houseNumber: consigneeData.houseNumber,
        postalCode: consigneeData.postalCode,
      },
      estimatedDeliveryDate: {
        pickupDate,
        pickupTime: estimatedDeliveryDate.pickupTime,
      }

    };

  }

  public normalizeOrder(order: CompareRequest, selectedService: CompareResponseData): CompareRequest {

    const { shipperData, consigneeData } = order;
    const { fromCountry, toCountry, fromCity = order.shipperData.city, toCity = order.consigneeData.city } = selectedService;



    return Object.assign(
      {},
      order,
      {shipperData: {...shipperData, country: fromCountry, city: fromCity}},
      {consigneeData: {...consigneeData, country: toCountry, city: toCity}},
    );
  }

}
