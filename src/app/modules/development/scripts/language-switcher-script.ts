import { LanguageObserver } from '@app/utils/observers';

export function devLanguageSwitcher() {

  document.addEventListener('keydown', event => {
    if (event.altKey) {

      if (event.key === 'l') {
        LanguageObserver.next({code: 'en', isRtl: false, nativeCode: 'en'});
      } else if (event.key === 'r') {
        LanguageObserver.next({code: 'he', isRtl: true, nativeCode: 'עברית'});
      }
    }
  });

}
