import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { ClearCacheComponent, AutofillComponent } from './components';
import { OnlyDevDirective } from './directives/only-production.directive';


@NgModule({
  declarations: [ClearCacheComponent, AutofillComponent, OnlyDevDirective],
  imports: [
    CommonModule,
    MatIconModule
  ],

  exports: [ClearCacheComponent, AutofillComponent, OnlyDevDirective]
})

export class DevelopmentModule { }
