
export const OPTION_CARD_DATA = [
  {bg: '#2980B9', partner: 2, timePoint: 'hours', amount: 24, fromType: 'pickup', toType: 'delivery' },
  {bg: '#FFB31A', partner: 3, timePoint: 'hours', amount: 48, fromType: 'drop off', toType: 'delivery' },
  {bg: '#183E6F', partner: 5, timePoint: 'days', amount: 3, fromType: 'drop off', toType: 'pickup' },
  {bg: '#183E6F', partner: 1, timePoint: 'days', amount: 4, fromType: 'drop off', toType: 'delivery' },
];
