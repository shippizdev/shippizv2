import { Component, HostListener } from '@angular/core';
import { CacheService, AuthService } from '@app/services';
import { environment } from 'environments/environment';

@Component({
  selector: 'dev-clear-cache',
  templateUrl: './clear-cache.component.html',
  styleUrls: ['./clear-cache.component.scss']
})
export class ClearCacheComponent {


  constructor(private cache: CacheService) {}

  @HostListener('click')
  clearCache() {
    this.cache.clear();
    alert('Cache cleared');
  }

}
