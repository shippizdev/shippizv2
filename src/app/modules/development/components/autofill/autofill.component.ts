import { Component, HostListener, Input } from '@angular/core';
import { DevAutoFillService } from '../../services/dev-auto-fill.service';
import { FormGroup } from '@angular/forms';
import { Locations } from '@app/interfaces';

@Component({
  selector: 'dev-autofill',
  templateUrl: './autofill.component.html',
  styleUrls: ['./autofill.component.scss'],
  providers: [DevAutoFillService]
})
export class AutofillComponent {


  @Input() formName: string;
  @Input() targetForm: FormGroup;
  @Input() locations: Locations;

  constructor(private devAutoFill: DevAutoFillService) {}

  @HostListener('click')
  autoFill() {

    switch (this.formName) {
      case 'compare':
        this.devAutoFill.autoFillCompare(this.targetForm, this.locations);
        break;
      case 'pickup':
        this.devAutoFill.autoFillPickup(this.targetForm);
        break;
      case 'payment':
        this.devAutoFill.autoFillPayment(this.targetForm);
        break;
    }
  }

}
