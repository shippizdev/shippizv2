import { Injectable } from '@angular/core';
import { PACKAGE_DETAIL_TEMPLATE, LOCATIONS_TOKEN } from '@app/constants';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { LocalStorageService } from '@app/services';
import { Locations } from '@app/interfaces';

@Injectable()
export class DevAutoFillService {

  constructor(private builder: FormBuilder, private store: LocalStorageService) { }

  autoFillCompare(form: FormGroup, locations: Locations): void {

    const packageDetails = form.get('packageDetails') as FormArray;
    packageDetails.clear();
    packageDetails.push(this.builder.group(PACKAGE_DETAIL_TEMPLATE));
    packageDetails.push(this.builder.group(PACKAGE_DETAIL_TEMPLATE));

    form.patchValue({
      origin: {
        country: 'IL',
        city: 'Tel Aviv-Yafo',
        state: '',
        street: 'Allenby Street',
        houseNumber: '9',
        postalCode: '6713200',
      },

      destination: {
        country: 'US',
        city: 'New York',
        state: 'NY',
        street: 'Park Avenue',
        houseNumber: '5',
        postalCode: '10170',
      },

      measurementUnits: 1,
      shippingTypeId: 2,
      packageDetails: [
        { weight: '3.15', quantity: '2', length: '30', width: '40', height: '20', description: 'test' },
        { weight: '2', quantity: '1', length: '25', width: '20', height: '20', description: 'test' },
      ],
    });

    const mockLocations = {
      org: 'Allenby Street 9 Tel Aviv-Yafo, IL',
      dest: '5 Park Avenue New York , NY, USA',
    };

    this.store.set<Locations>(LOCATIONS_TOKEN, locations);
    Object.assign(locations, mockLocations);

  }

  autoFillPickup(form: FormGroup): void {

    form.patchValue({

      documentDescription: 'example description',
      Vat_TaxId: 'example tax ID',
      estimatePackageValue: '100',
      currency: 'USD',
      reason: 'Gift',

      shipperData: {
        name: 'Microsoft',
        contactPerson: 'Kobi',
        phone: '123456789',
        email: 'kobi@gmail.com',
        additionalInstructions: 'example additional instructions',
      },

      consigneeData: {
        name: 'Amazone',
        contactPerson: 'Rafayel',
        phone: '987654321',
        email: 'rafayel@gmail.com',
        additionalInstructions: 'example additional instructions',
      },

    });
  }

  autoFillPayment(form: FormGroup): void {
    form.patchValue({
      fName: 'kobi',
      lName: 'cohen',
      ID: '123456789',
      currency: 'USD',
      ccNumber: '12312312',
      cvv: '458',
      expDate: '12/20',
    });

  }
}
