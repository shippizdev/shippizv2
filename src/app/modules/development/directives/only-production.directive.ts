import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { environment } from 'environments/environment';
import { AuthService } from '@app/services';
import { API } from '@app/classes';

@Directive({
  selector: '[appOnlyDev]'
})
export class OnlyDevDirective {

  private isDev: boolean;
  private hasView: boolean;

  constructor(
    private auth: AuthService,
    private tr: TemplateRef<any>,
    private vcr: ViewContainerRef,
  ) {

    this.auth.currentUser.subscribe(user => {

      this.isDev = (user && !!user.testUser) || !environment.production || API.__enviorment === 'local';

      if (this.isDev && !this.hasView) {

        this.vcr.createEmbeddedView(this.tr);
        this.hasView = true;
      } else if (!this.isDev && this.hasView) {

        this.vcr.clear();
        this.hasView = false;
      }
    });


  }

}
