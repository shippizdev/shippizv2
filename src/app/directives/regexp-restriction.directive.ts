import { Directive, Input, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appRegexpRestriction]'
})

export class RegexpRestrictionDirective {

  constructor(private controller: NgControl) {}

  @Input('appRegexpRestriction') regexp: RegExp;
  lastValidValue: string;

  @HostListener('input', ['$event.target'])
  whileTyping(target: HTMLInputElement | HTMLTextAreaElement) {

    const passed: boolean = this.regexp.test(target.value) || !target.value;

    if (!passed) {
      target.value = this.lastValidValue;
      this.controller.control.setValue(this.lastValidValue);
    }

  }

  @HostListener('keydown', ['$event.target'])
  onKeystorke(target: HTMLInputElement | HTMLTextAreaElement) {
    this.lastValidValue = target.value;
  }

}
