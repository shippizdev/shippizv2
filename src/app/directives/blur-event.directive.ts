import { Directive, Input, Output, EventEmitter, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appBlurEvent]'
})
export class BlurEventDirective {
  @Output()
  appBlurEvent = new EventEmitter<void>();

  constructor(private elementRef: ElementRef) {  }

  @HostListener('document:click', ['$event.target'])
    onClick(el) {
      const clickedInside = this.elementRef.nativeElement.contains(el);
      if (!clickedInside) {
        this.appBlurEvent.emit();
      }
    }
}
