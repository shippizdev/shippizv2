import { Directive, AfterViewInit, ElementRef, NgZone, OnDestroy, Output, EventEmitter } from '@angular/core';
import { GooglePlace, GoogleAutocompleteRef, GooglePlaceChangeListener } from '@app/interfaces';
import { GoogleAutocompleteService } from '@app/services';


@Directive({
  selector: '[appGoogleAutocomplete]'
})

export class GoogleAutocompleteDirective implements AfterViewInit, OnDestroy {

  private placeChangedListener: GooglePlaceChangeListener;
  private autocompleteRef: GoogleAutocompleteRef;
  private target: HTMLInputElement;

  @Output() placeChanged = new EventEmitter<GooglePlace>();
  @Output() postalCodeChanged = new EventEmitter<string>();


  constructor(
    elRef: ElementRef,
    private ngZone: NgZone,
    private googleAutocomplete: GoogleAutocompleteService) {

    this.target = elRef.nativeElement;

  }

  ngAfterViewInit() {

    this.autocompleteRef = new google.maps.places.Autocomplete(this.target);
    this.autocompleteRef.setFields(['address_components', 'formatted_address', 'geometry', 'adr_address']);
    this.placeChangedListener = this.autocompleteRef.addListener('place_changed', () => {

      this.ngZone.run(async () => {

        const place: GooglePlace = this.autocompleteRef.getPlace();
        this.placeChanged.emit(place);

        let postalCode = await this.googleAutocomplete.retrievePostalCode(place);

        const reg = /^\w+$/;
        postalCode = (postalCode && reg.test(postalCode)) ? postalCode : '';
        this.postalCodeChanged.emit(postalCode);
      });
    });
  }

  ngOnDestroy() {
    this.placeChangedListener.remove();
  }

}
