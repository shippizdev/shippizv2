import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appStopPropagationEvent]'
})
export class StopPropogetionEventDirective {

  constructor() {}

  @HostListener('click', ['$event'])
  onClick(event) {
    event.stopPropagation();
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event) {
    event.stopPropagation();
  }
}
