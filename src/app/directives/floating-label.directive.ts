import { Directive, ElementRef, HostBinding } from '@angular/core';

@Directive({
  selector: '[appFloatingLabel]'
})
export class FloatingLabelDirective {

  constructor(private elRef: ElementRef) {}

  @HostBinding('class.cst-filled')
  get hasValue() {
    return this.elRef.nativeElement.value;
  }

}
