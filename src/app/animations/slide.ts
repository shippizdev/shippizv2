import { trigger, state, style, transition, animate } from '@angular/animations';

export const Slide = [
  trigger('slide', [
    state('begin', style({
      right: 0,
      transform: 'translate(100%, -50%) scale(0.5)',
      opacity: 0.5,
    })),
    state('middle', style({
      right: '125px',
      transform: 'translate(50%, -50%) scale(2)',
      opacity: 1,
    })),
    state('end', style({
      right: '250px',
      transform: 'translate(0, -50%) scale(0.5)',
      opacity: 0.5,
    })),
    transition('begin => middle', [
      animate('0.6s')
    ]),

    transition('middle => end', [
      animate('0.6s')
    ]),

    transition('end => begin', [
      animate('0.6s')
    ]),
  ]),
];
