import { trigger, transition, style, animate } from '@angular/animations';

export const Expanding = [
  trigger('expanding', [
      transition(':enter', [
          style({height: 0, opacity: 0.2, margin: 0}),
          animate('300ms ease', style({height: '*', margin: '*', opacity: 1}))
      ]),
      transition(':leave', [
          animate('300ms ease', style({height: 0, opacity: 0.2, margin: 0}))
      ]),
  ])
];
