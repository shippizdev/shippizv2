import { trigger, transition, style, animate } from '@angular/animations';

const initialStyles = {transform: 'scale(0.9)', opacity: 0, height: 0, margin: 0, padding: 0};


export const RowInsertion = [
    trigger('rowInsertion', [
        transition(':enter', [
            style(initialStyles),
            animate('300ms ease', style({transform: 'scale(1)', opacity: 1, height: '*', margin: '*', padding: '*'}))
        ]),
        transition(':leave', [
            animate('300ms ease', style(initialStyles))
        ]),
    ])
];
