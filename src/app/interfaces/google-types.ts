type GoogleAutocompleteRef = google.maps.places.Autocomplete;
type GooglePlace = google.maps.places.PlaceResult;
type GooglePlaceChangeListener = google.maps.MapsEventListener;
type GoogleGeocoderResult = google.maps.GeocoderResult;

export {GoogleAutocompleteRef, GooglePlace, GooglePlaceChangeListener, GoogleGeocoderResult };
