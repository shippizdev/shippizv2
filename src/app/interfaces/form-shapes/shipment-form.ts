import { ContactInfo, DeliveryDateInfo } from '../models';

export interface ShipmentForm {

  documentDescription: string;
  Vat_TaxId: string;
  estimatePackageValue: string;
  currency: string;
  reason: string;
  receiveInvoice: string;
  payTaxes: string;

  shipperData: ContactInfo;
  consigneeData: ContactInfo;
  estimatedDeliveryDate: DeliveryDateInfo;
}
