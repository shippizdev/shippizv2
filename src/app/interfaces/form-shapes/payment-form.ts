export interface PaymentForm {
  fName: string;
  lName: string;
  ID: string;
  ccNumber: string;
  cvv: string;
  expDate: string;
}
