import { ContactInfo, PackageDetail } from '../models';

export interface CompareForm {
  shippingTypeId: string;
  measurementUnits: number;
  origin: ContactInfo;
  destination: ContactInfo;
  currency: string;
  packageDetails: PackageDetail[];
}
