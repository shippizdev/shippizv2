export class ContactInfo {
  name: string;
  country: string;
  state: string;
  city: string;
  street: string;
  houseNumber: string;
  postalCode: string;
  contactPerson: string;
  phone: string;
  email: string;
  additionalInstructions?: string;

  constructor({
    name = '',
    country = '',
    state = '',
    city = '',
    street = '',
    houseNumber = '',
    postalCode = '',
    contactPerson = '',
    phone = '',
    email = '',
    additionalInstructions = null,
  } = {}) {

    this.name = name;
    this.country = country;
    this.state = state;
    this.city = city;
    this.street = street;
    this.houseNumber = houseNumber;
    this.postalCode = postalCode;
    this.contactPerson = contactPerson;
    this.phone = phone;
    this.email = email;

    if (additionalInstructions) {
      this.additionalInstructions = additionalInstructions;
    }
  }
}
