import * as moment from 'moment';

export class DeliveryDateInfo {
  pickupDate: string;
  pickupTime: string;
  expectedTransit: string;
  deliveryBy: string;

  constructor({
    pickupDate = '',
    pickupTime = '',
    expectedTransit = '',
    deliveryBy = '',
  } = {}) {

    this.pickupDate = pickupDate;
    this.pickupTime = pickupTime;
    this.expectedTransit = expectedTransit;
    this.deliveryBy = deliveryBy;

    if (this.pickupDate) {
      this.pickupDate = moment(pickupDate).format('YYYY-MM-DD');
    }
    if (this.pickupTime) {
      this.pickupTime = moment(pickupTime, 'HH:mm:ss').format('HH:mm');
    }
  }
}
