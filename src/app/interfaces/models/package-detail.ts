export class PackageDetail {

  quantity: string;
  weight: string;
  height: string;
  length: string;
  width: string;
  description?: string;

  constructor({
    quantity = '',
    weight = '',
    height = '',
    length = '',
    width = '',
    description = null,
  } = {}) {

    this.quantity = quantity;
    this.weight = weight;
    this.height = height || '0';
    this.length = length || '0';
    this.width = width || '0';

    if (description) {
      this.description = description;
    }
  }
}
