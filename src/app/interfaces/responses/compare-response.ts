
export interface CompareResponseData {
  errMsg: string;
  processorId: string;
  price_USD: number;
  price_EUR: number;
  price_NIS: number;
  oiginalPrice_USD: number;
  oiginalPrice_EUR: number;
  oiginalPrice_NIS: number;
  fromCountry: string;
  toCountry: string;
  fromCity: string;
  toCity: string;
  deliveryTime: string;
  pickupDate: string;
  shippingTypeId: number;
  fuel: number;
  estimatedTimeNumber: string;
  estimatedPeriod: string;
  pickupStartTimeRange: string;
  pickupEndTimeRange: string;
  originalPrice?: string;
  totalPrice_USD: number;
  totalPrice_EUR: number;
  totalPrice_NIS: number;
  domestic_Country?: string;
}

export interface CompareResponseAdditional {
  weightToCharge: number;
  quantity: number;
  length: number;
  height: number;
  width: number;
  sizeType: string;
  weightType: string;
}

export interface CompareResponse {

  data: CompareResponseData[];
  additionalInfo: CompareResponseAdditional[];

}
