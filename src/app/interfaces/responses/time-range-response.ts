export interface TimeRangeResponseData {
  r: number;
  startTime: string;
  endTime: string;
}

export interface TimeRangeResponse {

  data: TimeRangeResponseData[][];
  additionalInfo: null;
}
