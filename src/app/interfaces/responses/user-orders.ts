import { ContactInfo } from '../models';

export interface UserOrderData {
  orderNumber: number;
  pickupAuthorization: string;
  processorId: number;
  processor: string;
  date: string;
  shipperData: ContactInfo;
  consigneeData: ContactInfo;
  amount: number;
  currency: string;
  deliveryDate: string;
  stasus: string;
  shippingAuthorization: string;
  labelURL: string;
}
