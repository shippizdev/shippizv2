export interface ShipmentResponseData {
  pickupId: number;
  trackingNumber: string;
  instruction: string;
}

export interface ShipmentResponse {
  data: ShipmentResponseData[];
  additionalInfo: null;
}
