export interface PickupResponseData {

  pickupId: number;
  pickupAuthorization: string;
  labelURL: string;
  paymentAmount: number;
  currency: string;
  weightToCharge: number;
  quantity: number;
  instruction: string;
  popupType: string;
  isNavigateToConfirmationPage: number;
}

export interface PickupResponse {
  data: PickupResponseData[][];
  additionalInfo: null;
}
