export interface Slide {
  src: string;
  heading: string;
  subheader: string;
  description: string;
  link: string;
}

interface Language {
  code: string;
  isRtl: boolean;
  nativeCode: string;
}

interface Locations {
  org: string;
  dest: string;
}

interface Country {
  countryId: number;
  name: string;
  countryCode: string;
}

interface City {
  cityId: number;
  city: string;
  countryCode: string;
}

interface Processor {
  processorId: number;
  name: string;
  originalName: string;
  logo: string;
  trackingURL: string;
  parent: string;
}

interface PackageType {
  id: number;
  name: string;
}

interface Currency {
  id: number;
  code: string;
  rate: number;
}

interface User {
  userId: number;
  sessionId: string;
  fName: string;
  lName: string;
  isMale: boolean;
  email: string;
  company: string;
  phone: string;
  country: string;
  city: string;
  address: string;
  money: number;
  currency: string;
  birthDate: string;
  profession: string;
  zip: string;
  isUpdateDetails: boolean;
  testUser?: number;
}

interface AuthFormState {
  mode: 'login' | 'registration' | null;
  skipable?: boolean;
  skipped?: boolean;
  highlight?: boolean;
}



export { Language, Locations, Country, City, Processor, PackageType, Currency, User, AuthFormState };
