import { ContactInfo, PackageDetail, DeliveryDateInfo } from '../models';

export interface ShipmentRequest {

  clientJson: {
    userId: string;
    sessionId: string;
    processorId: string;
    shippingTypeId: string;
    paymentAmount: string;
    currency: string;
    reason: string;
    documentDescription: string;
    Vat_TaxId: string;
    estimatePackageValue: string;
    weightType: string;
    sizeType: string;
    receiveInvoice: string;
    payTaxes: string;
    ydmExtraChargeForLocation?: number;

    shipperData: ContactInfo;
    consigneeData: ContactInfo;

    packageDetails: PackageDetail[];
    estimatedDeliveryDate: DeliveryDateInfo;
  };
}
