import { PaymentForm } from '../form-shapes';

export interface PickupRequest {
  clientJson: {
    userId: string | number;
    sessionId: string;
    processorId: string | number;
    pickupId: number;

    ydmId?: number;
    chargeFromBalance?: boolean;

    billing: {
      fName: string;
      lName: string;
      ID: string;
      ccNumber: string;
      cvv: string;
      expDate: string;
    },
  };
}
