import { ContactInfo, PackageDetail, DeliveryDateInfo } from '../models';

export interface CompareRequest {

  action: string;
  shippingTypeId: string;
  processorId: number;
  sizeType: string;
  weightType: string;
  currency: string;
  sessionId: string;

  shipperData: ContactInfo;
  consigneeData: ContactInfo;
  packageDetails: PackageDetail[];
  estimatedDeliveryDate: DeliveryDateInfo;
}
