
export interface TimeRangeRequest {
  processorId: number;
  shipperData: {
    country: string;
    city: string;
    state: string;
    street: string;
    houseNumber: string;
    postalCode: string;
  };
  consigneeData: {
    country: string;
    city: string;
    state: string;
    street: string;
    houseNumber: string;
    postalCode: string;
  };
  estimatedDeliveryDate: {
    pickupDate: string;
    pickupTime: string;
  };
}
