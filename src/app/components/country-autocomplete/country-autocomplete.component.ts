import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Country } from '@app/interfaces';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { CacheService } from '@app/services';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-country-autocomplete',
  templateUrl: './country-autocomplete.component.html',
  styleUrls: ['./country-autocomplete.component.scss']
})
export class CountryAutocompleteComponent implements OnInit {

  countryList: Country[] = [];
  countryAutocomplete$: Observable<Country[]>;
  countryControl = new FormControl('');

  @Input() isRequired = true;
  @Output() public countrySelected = new EventEmitter<string>();

  constructor(
    private cache: CacheService,
  ) {

    this.getNameByCode = this.getNameByCode.bind(this);
  }

  private normalize(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  private filter<T>(term: string, array: T[], targetProperty: string) {

    const normalizedTerm = this.normalize(term);

    return array.filter(item => {
      return this.normalize(item[targetProperty]).includes(normalizedTerm);
    });
  }


  public getNameByCode(code: string): string {
    if (this.countryList.length) {
      return this.countryList.find(country => country.countryCode === code).name;
    } else {
      return null;
    }
  }

  public remoteSet(country) {
    if (country) {
      this.countryControl.setValue(country);
      this.countrySelected.emit(country);
    }
  }

  async ngOnInit() {
    this.countryList = await this.cache.getCountries() as Country[];
    this.countryAutocomplete$ = this.countryControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter<Country>(value, this.countryList, 'name')),
    );
  }

}
