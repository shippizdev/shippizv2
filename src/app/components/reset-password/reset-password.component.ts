import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { BackendService } from '@app/services';
import { RowInsertion } from '@app/animations';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: [RowInsertion]
})
export class ResetPasswordComponent {

  email = new FormControl('', [Validators.required]);
  errMsg: string;
  successMsg: string;
  url = '';

  constructor(
    private dialogRef: MatDialogRef<ForgotPasswordComponent>,
    private backend: BackendService) { }


  close() {
    this.dialogRef.close();
  }

  resetPassword() {
    this.backend.execute('ResetPassword', {email: this.email.value, ip: ''})
    .then(({data}) => {

      data = data[0][0];


      this.successMsg = data.instructions.substring(0, data.instructions.search(/http/));
      this.url = data.instructions.substring(data.instructions.search(/http/));
    })
    .catch(err => {
      this.errMsg = err;
      setTimeout(() => this.errMsg = null, 5000);
    });
  }

}
