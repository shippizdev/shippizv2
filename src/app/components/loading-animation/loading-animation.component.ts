import { Component, OnInit } from '@angular/core';
import { Slide } from '@app/animations/slide';

@Component({
  selector: 'app-loading-animation',
  templateUrl: './loading-animation.component.html',
  styleUrls: ['./loading-animation.component.scss'],
  animations: [Slide]
})
export class LoadingAnimationComponent implements OnInit {

  activeIndex: number;

  images: string[] = [
    'assets/img/partners/usps.png',
    'assets/img/partners/dhl.png',
    // 'assets/img/partners/tnt.png',
    // 'assets/img/partners/fedex.png'
  ];

  constructor() { }

  ngOnInit() {
    let i = 0;
    setInterval(() => {
      this.activeIndex = i % this.images.length;
      i++;
    }, 600);
  }

}
