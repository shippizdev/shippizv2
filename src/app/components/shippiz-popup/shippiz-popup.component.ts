import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shippiz-popup',
  templateUrl: './shippiz-popup.component.html',
  styleUrls: ['./shippiz-popup.component.scss']
})
export class ShippizPopupComponent implements OnInit {

  @Input() action = () => {};
  @Input() actionName = 'OK';
  @Input() dismissable = true;
  @Input() heading: string;

  constructor() { }

  ngOnInit() {
  }

}
