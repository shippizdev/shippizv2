import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CacheService, BackendService } from '@app/services';
import { Country, City } from '@app/interfaces';
import { startWith, map, pairwise } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-autocomplete-combo-box',
  templateUrl: './autocomplete-combo-box.component.html',
  styleUrls: ['./autocomplete-combo-box.component.scss']
})
export class AutocompleteComboBoxComponent implements OnInit {



  countryList: Country[] = [];
  cityList: City[] = [];

  countryAutocomplete$: Observable<Country[]>;
  cityAutocomplete$: Observable<City[]>;

  countryControl = new FormControl('');
  cityControl = new FormControl('');

  @Input() isRequired = true;
  @Output() public countrySelected = new EventEmitter<string>();
  @Output() public citySelected = new EventEmitter<string>();

  constructor(
    private cache: CacheService,
    private backend: BackendService,
  ) {

    this.getNameByCode = this.getNameByCode.bind(this);
  }

  private normalize(value: string): string {
    return value.toLowerCase();
  }

  private filter<T>(term: string, array: T[], targetProperty: string) {

    const normalizedTerm = this.normalize(term);

    return array.filter(item => {
      return this.normalize(item[targetProperty]).includes(normalizedTerm);
    });
  }


  private getCityList(countryCode: string, cityPrefix: string): Promise<any> {
    if (!countryCode) {
      return Promise.resolve([]);
    }
    const payload = {countryCode, city: cityPrefix};
    return this.backend.execute('GetCitiesByCountryCode', payload);
  }

  public getNameByCode(code: string): string {
    if (this.countryList.length) {
      return this.countryList.find(country => country.countryCode === code).name;
    } else {
      return null;
    }
  }

  public remoteSet({country = null, city = null} = {}) {
    if (country) {
      this.countryControl.setValue(country);
      this.countrySelected.emit(country);
    }
    if (city) {
      this.cityControl.setValue(city);
      this.citySelected.emit(city);
    }
  }

  public refreshObservers() {
    this.getCityList(this.countryControl.value, this.cityControl.value.slice(0, 2))
    .then(response => this.cityList = response);
  }

  async ngOnInit() {

    this.countryList = await this.cache.getCountries() as Country[];

    this.countryAutocomplete$ = this.countryControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter<Country>(value, this.countryList, 'name')),
    );

    this.cityAutocomplete$ = this.cityControl.valueChanges.pipe(
      startWith(''),
      map(value => {
        return this.filter<City>(value, this.cityList, 'city');
      }),
    );

    this.cityControl.valueChanges
    .pipe(pairwise())
    .subscribe(async ([prePrefix, postPrefix]) => {
      if (postPrefix.length === 2 ||
        (postPrefix.length > 2 && prePrefix.toLowerCase().slice(0, 2) !== postPrefix.toLowerCase().slice(0, 2))) {
        const {data} = await this.getCityList(this.countryControl.value, postPrefix);
        this.cityList = data[0] || [];
      } else if (postPrefix.length < 2) {
        this.cityList = [];
      }
    });
  }

}
