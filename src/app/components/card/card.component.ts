import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Slide } from '@app/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() slide: Slide;
  isHovered: boolean;
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  @HostListener('mouseenter')
  onEnter() {
    this.isHovered = true;
  }

  @HostListener('mouseleave')
  onLeave() {
    this.isHovered = false;
  }

  naviagte(link: string) {

    switch (link) {
      case 'ship now':
        if (this.router.url.includes('home')) {
          window.scrollTo({
            top: 0,
            behavior: 'smooth',
          });
        } else {
          this.router.navigate(['/home']);
        }
        break;
      case 'contact us':
        this.router.navigate(['/contact_us']);
    }

  }

}
