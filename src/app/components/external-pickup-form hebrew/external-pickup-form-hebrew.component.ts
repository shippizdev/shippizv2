import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { BackendService, AuthService } from '@app/services';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';
import { NotificationService } from '@app/services/notification.service';

@Component({
  selector: 'app-external-pickup-form-hebrew',
  templateUrl: './external-pickup-form-hebrew.component.html',
  styleUrls: ['./external-pickup-form-hebrew.component.scss'],
})
export class ExternalPickupFormHebrewComponent implements OnInit {

  pickupForm: FormGroup;
  paymentForm: FormGroup;

  successMessage: string;
  link: string;
  id: number;
  token: string;
  needToPay = 0;

  monthOptions$: Observable<number[]>;
  yearOptions: number[];

  constructor(
    private notification: NotificationService,
    private auth: AuthService,
    private builder: FormBuilder,
    private backend: BackendService,
    private active: ActivatedRoute,
  ) {}

  async ngOnInit() {

    const yearOptionsAmount = 15;
    const currentYear = +moment().format('YY');
    const currentMonth = +moment().format('M');

    this.yearOptions = [];

    for (let i = currentYear; i <= currentYear + yearOptionsAmount; i++) {
      this.yearOptions.push(i);
    }

    const allMonthOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    const currentYearMonthOptions = [];

    for (let i = currentMonth; i <= 12; i++) {
      currentYearMonthOptions.push(i);
    }

    this.paymentForm = this.builder.group({

      fName: [''],
      lName: [''],
      ID: [''],
      ccNumber: [''],
      cvv: [''],
      expDateMonth: [currentMonth.toString()],
      expDateYear: [currentYear.toString()],

    });

    this.paymentForm.valueChanges.subscribe(console.log);

    this.monthOptions$ = this.paymentForm.get('expDateYear').valueChanges.pipe(
      startWith(currentYear),
      map(value => +value === currentYear ? currentYearMonthOptions : allMonthOptions),
    );

    this.paymentForm.valueChanges.subscribe(({expDateMonth, expDateYear}) => {
      if (expDateYear === currentYear.toString() && !currentYearMonthOptions.includes(+expDateMonth)) {
        this.paymentForm.get('expDateMonth').setValue(currentMonth.toString());
      }
    });

    this.pickupForm  = this.builder.group({

      packageDetails: this.builder.array([
        this.builder.group({
          weight: [''],
          quantity: [''],
        })
      ]),


      shipperData: this.builder.group({
        name : [''],
        city: [''],
        street : [''],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : [''],
        phone : [''],
        email : [''],
        additionalInstructions: [''],
      }),

      consigneeData: this.builder.group({
        name : [''],
        city: [''],
        street : [''],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : [''],
        phone : [''],
        email : [''],
        additionalInstructions: [''],
      }),

    });

    const snapshot = this.active.snapshot.paramMap;
    this.id = Number(snapshot.get('id'));
    this.token = snapshot.get('token');

    if (!this.id && !this.token) {
      return;
    }

    const response = await this.loadTemplate(this.id, this.token);
    const formValue = JSON.parse(response.data[0][0].clientJson);

    while (formValue.packageDetails.length > this.packages.length) {
      this.addPackage();
    }

    this.pickupForm.setValue(formValue);

  }


  get packages() {
    return this.pickupForm.get('packageDetails') as FormArray;
  }

  addPackage() {
    this.packages.push(this.builder.group({
      weight: [''],
      quantity: [''],
    }));
  }

  clearPackages() {
    this.packages.controls.forEach(pack => pack.reset());
  }

  async save() {

    if (!this.id && !this.token) {
      const {id, token, link} = await this.generateTemplateLink();
      this.id = id;
      this.token = token;
      this.link = link;
    }

    const {instruction, needToPay} = await this.saveTemplate(this.id, this.token, this.pickupForm.value);
    this.successMessage = instruction;
    this.needToPay = needToPay;

  }

  async pay() {
    const {fName, lName, ID, ccNumber, cvv, expDateYear, expDateMonth} = this.paymentForm.value;

    const payload = {
      clientJson: {
        userId: null,
        sessionId: null,
        processorId: 20,
        ydmId: this.id,
        pickupId: -1,
        billing: {
          fName,
          lName,
          ID,
          ccNumber,
          cvv,
          expDate: expDateMonth + '/' + expDateYear,
        }
      }
    };

    try {
      await this.backend.executePickup(payload);
      this.notification.successNotification('Payment Success');
    } catch {
      this.notification.errorNotification('Payment Error');
    }
  }

  async generateTemplateLink() {
    const response = await this.backend.execute('GenerateShipmentTemplate', {
      formType: 'ydm'
    });
    return response.data[0][0];
  }

  async saveTemplate(id: number, token: string, clientJson: any) {
    const response = await this.backend.execute('SaveShipmentTemplate', {id, token, clientJson});
    return response.data[0][0];
  }

  async loadTemplate(id: number, token: string) {
    const response = await this.backend.execute('LoadShipmentTemplate', {id, token});
    return response;
  }

  copyLink() {
    try {
      navigator.clipboard.writeText(this.link);
    } catch {
      alert(
        `Your platform doesn't support clipboard manipulations.

        Here is your link:
        ${this.link}`
      );
    }
  }

}
