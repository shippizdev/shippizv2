import { Expanding } from './../../animations/expanding';
import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-expandable-content',
  templateUrl: './expandable-content.component.html',
  styleUrls: ['./expandable-content.component.scss'],
  animations: [Expanding]
})
export class ExpandableContentComponent implements OnInit {
@Input() isOdd: boolean;
@Input() subHeader: string;
isExpanded: boolean = false;
  constructor() { }

  ngOnInit() {
  }


}
