import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import DIAL_CODES from 'assets/dial-codes.json';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, tap } from 'rxjs/operators';

interface DialCode {
  code: string;
  name: string;
  dial_code: string;
}

@Component({
  selector: 'app-phone-autocomplete',
  templateUrl: './phone-autocomplete.component.html',
  styleUrls: ['./phone-autocomplete.component.scss']
})
export class PhoneAutocompleteComponent implements OnInit {

  dialCodes: DialCode[];
  phoneAutocomplete$: Observable<DialCode[]>;
  phoneControl: FormControl = new FormControl();
  @Input() code: string;
  @Output() codeSelected = new EventEmitter<string>();

  constructor() {}

  private normalize(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  private filter<T>(term: string, array: T[], targetProperty: string) {

    const normalizedTerm = this.normalize(term);

    return array.filter(item => {
      return this.normalize(item[targetProperty]).includes(normalizedTerm);
    });
  }

  ngOnInit() {
    this.dialCodes = DIAL_CODES.sort((a, b) => parseInt(a.dial_code, 10) - parseInt(b.dial_code, 10));
    this.phoneAutocomplete$ = this.phoneControl.valueChanges.pipe(
      startWith(''),
      map(value => this.filter<DialCode>(value, this.dialCodes, 'dial_code')),
    );

    this.phoneControl.setValue(this.getPhoneCode(this.code) || this.code.toString());
    this.codeSelected.emit(this.phoneControl.value);
  }

  getPhoneCode(countryCode: string) {
    const obj = this.dialCodes.find(item => item.code === countryCode);
    return obj && obj.dial_code;
  }

}
