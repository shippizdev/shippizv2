import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { BackendService } from '@app/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-external-pickup-form',
  templateUrl: './external-pickup-form.component.html',
  styleUrls: ['./external-pickup-form.component.scss'],
})
export class ExternalPickupFormComponent implements OnInit {

  pickupForm: FormGroup;
  successMessage: string;
  link: string;
  id: number;
  token: string;
  resolved = false;

  constructor(
    private builder: FormBuilder,
    private backend: BackendService,
    private active: ActivatedRoute,
  ) {}

  async ngOnInit() {

    this.pickupForm  = this.builder.group({

      packageDetails: this.builder.array([
        this.builder.group({
          weight: [''],
          quantity: [''],
          length: [''],
          width: [''],
          height: [''],
        })
      ]),

      measurementUnits: [1],
      documentDescription: [''],
      Vat_TaxId : [''],
      estimatePackageValue: [''],
      reason: [''],
      receiveInvoice: 'sender',
      payTaxes: 'receiver',

      shipperData: this.builder.group({
        name : [''],
        country: [''],
        state : [''],
        city: [''],
        street : [''],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : [''],
        phoneCode : [''],
        phone : [''],
        email : [''],
        additionalInstructions: [''],
      }),

      consigneeData: this.builder.group({
        name : [''],
        country: [''],
        state : [''],
        city: [''],
        street : [''],
        houseNumber : [''],
        postalCode : [''],
        contactPerson : [''],
        phoneCode : [''],
        phone : [''],
        email : [''],
        additionalInstructions: [''],
      }),

    });

    const snapshot = this.active.snapshot.paramMap;
    this.id = Number(snapshot.get('id'));
    this.token = snapshot.get('token');

    if (!this.id && !this.token) {
      this.resolved = true;
      return;
    }

    const response = await this.loadTemplate(this.id, this.token);
    const formValue = JSON.parse(response.data[0][0].clientJson);

    while (formValue.packageDetails.length > this.packages.length) {
      this.addPackage();
    }

    this.pickupForm.setValue(formValue);
    this.resolved = true;

  }

  get units() {
    return this.pickupForm.get('measurementUnits') as FormControl;
  }

  get packages() {
    return this.pickupForm.get('packageDetails') as FormArray;
  }

  addPackage() {
    this.packages.push(this.builder.group({
      weight: [''],
      quantity: [''],
      length: [''],
      width: [''],
      height: [''],
      description: [''],
    }));
  }

  switchMeasurementUnits() {
    if (this.units.value === 1) {
      this.units.setValue(2);
    } else {
      this.units.setValue(1);
    }
  }

  clearPackages() {
    this.packages.controls.forEach(pack => pack.reset());
  }

  async save() {

    if (!this.id && !this.token) {
      const {id, token, link} = await this.generateTemplateLink();
      this.id = id;
      this.token = token;
      this.link = link;
    }

    const {instruction} = await this.saveTemplate(this.id, this.token, this.pickupForm.value);
    this.successMessage = instruction;

  }

  async generateTemplateLink() {
    const response = await this.backend.execute('GenerateShipmentTemplate', null);
    return response.data[0][0];
  }

  async saveTemplate(id: number, token: string, clientJson: any) {
    const response = await this.backend.execute('SaveShipmentTemplate', {id, token, clientJson});
    return response.data[0][0];
  }

  async loadTemplate(id: number, token: string) {
    const response = await this.backend.execute('LoadShipmentTemplate', {id, token});
    return response;
  }

  copyLink() {
    try {
      navigator.clipboard.writeText(this.link);
    } catch {
      alert(
        `Your browser doesn't support clipboard manipulating.
        Here is your link: ${this.link}`
      );
    }
  }

}
