import { NotificationService } from '../../services/notification.service';
import { Component, OnInit, Input } from '@angular/core';
import { UserOrderData } from '@app/interfaces/responses/user-orders';
import { Processor } from '@app/interfaces';
import { CacheService, BackendService, ConverterService, LocalStorageService } from '@app/services';
import { InvoiceComponent } from '../invoice/invoice.component';
import { InvoiceManagerService } from '@app/services/invoice-manager.service';
import * as moment from 'moment';
import { ORDER_DATA_TOKEN, SHIPMENT_DATA_TOKEN, LOCATIONS_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN, NORMALIZED_ORDER_DATA_TOKEN } from '@app/constants';
import { Router } from '@angular/router';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-delivery-report',
  templateUrl: './delivery-report.component.html',
  styleUrls: ['./delivery-report.component.scss']
})
export class DeliveryReportComponent implements OnInit {

  @Input() order: UserOrderData;
  @Input() isOdd: boolean;
  @Input() isFirst: boolean;
  orderProcessor: Processor;
  statusColor;
  date ;

  constructor(
    private router: Router,
    private store: LocalStorageService,
    private cache: CacheService,
    private backend: BackendService,
    private converter: ConverterService,
    private notification: NotificationService,
    private invoiceMng: InvoiceManagerService
    ) { }

  cancelShipment() {

    const json = this.converter.convertToDeletePickUpRequest(this.order);

    this.notification.openConfirmPopup([
      'You are about to cancel this pickup,',
      'Are you sure that you want to continue?'
    ], () => {
      this.backend.executeDeletePickup(json)
      .then(res => {
       this.notification.successNotification(res.data[0][0].instruction);
       this.order.stasus = 'Canceled';
      })
      .catch(err => {
       this.notification.errorNotification(err);
      });
    });
  }

  openInvoice() {
    this.invoiceMng.openInvoice(this.order.orderNumber);
  }

  async repeatShipment() {
    const response = await this.backend.execute('RepeatShipment', {
      historyType: 'all',
      pickupId: this.order.orderNumber,
    });

    const data = JSON.parse(response.data[0][0].clientJson);

    const {
      shippingTypeId,
      processorId,
      sizeType,
      weightType,
      currency,
      sessionId,
      shipperData,
      consigneeData,
      packageDetails,
      estimatedDeliveryDate,

      documentDescription,
      Vat_TaxId,
      estimatePackageValue,
      reason,
      receiveInvoice,
      payTaxes,
    } = data;

    let orgLocation;
    let destLocation;
    {
      const {houseNumber, street, city, state, country} = shipperData;
      orgLocation = [houseNumber, street, city, state, country];
      orgLocation = orgLocation.filter(segment => segment);
      orgLocation = orgLocation.join(', ');
    }

    {
      const {houseNumber, street, city, state, country} = consigneeData;
      destLocation = [houseNumber, street, city, state, country];
      destLocation = destLocation.filter(segment => segment);
      destLocation = destLocation.join(', ');
    }

    this.store.set(LOCATIONS_TOKEN, {
      org: orgLocation,
      dest: destLocation
    });

    this.store.set(LOCATIONS_TOKEN, {
      org: orgLocation,
      dest: destLocation
    }, true);

    this.store.set(ORDER_DATA_TOKEN, {
      action: 'CompareRates',
      shippingTypeId,
      processorId,
      sizeType,
      weightType,
      currency,
      sessionId,
      shipperData,
      consigneeData,
      packageDetails,
      estimatedDeliveryDate
    });

    this.store.set(ORDER_DATA_TOKEN, {
      action: 'CompareRates',
      shippingTypeId,
      processorId,
      sizeType,
      weightType,
      currency,
      sessionId,
      shipperData,
      consigneeData,
      packageDetails,
      estimatedDeliveryDate
    }, true);

    this.store.set(SHIPMENT_DATA_TOKEN, {
      documentDescription,
      Vat_TaxId,
      estimatePackageValue,
      reason,
      receiveInvoice,
      payTaxes,
      currency,
      shipperData: {...shipperData, phone: shipperData.phone.replace(/.*?-/, '')},
      consigneeData: {...consigneeData, phone: consigneeData.phone.replace(/.*?-/, '')},
      estimatedDeliveryDate
    });

    const res = await this.backend.executeCompare(this.store.get(ORDER_DATA_TOKEN));

    this.store.set('__PACKAGE__DETAILS__', res.additionalInfo[0]);
    this.store.set(SELECTED_PROCESSOR_DATA_TOKEN, res.data[0]);

    this.store.set(NORMALIZED_ORDER_DATA_TOKEN, this.converter.normalizeOrder(this.store.get(ORDER_DATA_TOKEN), res.data[0]));

    this.router.navigate(['/ship', 'address_details']);

  }

  track() {
    const url = this.orderProcessor.trackingURL;
    window.open(url + this.order.shippingAuthorization);
  }


  async ngOnInit() {
    this.date = moment(this.order.date, 'YYYY-MM-DD').format('DD/MM/YYYY');
    this.orderProcessor = await this.cache.getProcessorType(this.order.processorId) as Processor;
  }

}
