import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BackendService } from '@app/services';
import { passwordConfirm } from '@app/utils/validators';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  token: string;
  email: string;
  errMsg: string;
  passVisibility: boolean;

  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private backend: BackendService,
    private router: Router) { }

  updatePassword() {

    this.backend.execute('UpdatePassword', {email: this.email, token: this.token, password: this.changePasswordForm.get('password').value})
    .then(({data}) => {
      alert(data[0][0].instruction);
      this.router.navigate(['/home']);
    })
    .catch(err => {
      this.errMsg = err;
    });
  }

  ngOnInit() {
    this.token = this.activeRoute.snapshot.queryParamMap.get('token');
    this.email = this.activeRoute.snapshot.queryParamMap.get('email');

    this.changePasswordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]],
      passwordConfirm: ['', Validators.required],
    }, {validators: passwordConfirm});

    this.backend.execute('CheckValidResetPasswordCredentials', {email: this.email, token: this.token})
    .catch(err => {
      this.errMsg = err;
      this.changePasswordForm.disable();
    });
  }


}
