import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Processor, CompareResponseData } from '@app/interfaces';
import { CacheService, LocalStorageService } from '@app/services';
import { Router } from '@angular/router';
import { MatExpansionPanel } from '@angular/material';
import { SELECTED_PROCESSOR_DATA_TOKEN } from '@app/constants';
import { CurrencyObserver } from '@app/utils/observers';

const BG_COLORS = ['theme-light-orange-bg', 'theme-light-blue-bg', 'theme-dark-blue-bg'];

@Component({
  selector: 'app-option-card',
  templateUrl: './option-card.component.html',
  styleUrls: ['./option-card.component.scss']
})
export class OptionCardComponent implements OnInit {

  activeNumber: number;
  currencyObserver = CurrencyObserver;


  @ViewChild(MatExpansionPanel, {static: false})
  panel: MatExpansionPanel;


  constructor(
    private cache: CacheService,
    private store: LocalStorageService,
    private router: Router) { }

  @Input() option: CompareResponseData;
  @Input() index: number;

  bgClass: string;
  processor: Promise<Processor>;

  ngOnInit() {
    this.activeNumber = 2;
    this.bgClass = BG_COLORS[this.index % BG_COLORS.length];
    this.processor = this.cache.getProcessorType(+this.option.processorId) as Promise<Processor>;
  }


  changeActiveNum(activeId: 1 | 2 | 3, event: MouseEvent) {

    this.activeNumber = activeId;
    if (this.panel.expanded) {
      event.stopPropagation();
    }
  }

  chooseService() {
    this.store.set<CompareResponseData>(SELECTED_PROCESSOR_DATA_TOKEN, this.option);

    if (this.option.domestic_Country) {
      this.router.navigate(['/ship', `domestic_pickup_${this.option.domestic_Country}`]);
    } else {
      this.router.navigate(['/ship', 'address_details']);
    }
  }

}
