import { Component, OnInit, Inject, Optional, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Processor } from '@app/interfaces';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FLOAT_REGEXP, INTEGER_REGEXP } from '@app/constants';
import { CacheService, AuthService, BackendService } from '@app/services';
import printJS from 'print-js';
import { toObject } from '@app/utils/functions';
import moment from 'moment';

const TABLE_ROW_TEMPLATE = {
  serialNo: [''],
  numberOfPackages: [''],
  weight: [''],
  description: [''],
  quantity: [''],
  unitValue: [''],
  valueForCustomsOnly: [''],
  currency: [''],
};

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})

export class InvoiceComponent implements OnInit {

  floatReg = FLOAT_REGEXP;
  intReg = INTEGER_REGEXP;

  invoiceTableForm: FormGroup;
  processorComboBox: FormControl;

  from: any;
  to: any;
  date: string;
  currency: string;
  pickupId: number;
  misc: any;

  processors: Processor[] = [];
  processorId: number;
  processorName: string;

  printing = false;
  requesting = false;

  @ViewChildren('CellRef')
  cellRefs: QueryList<ElementRef<HTMLTableCellElement>>;

  constructor(
    private dialogRef: MatDialogRef<InvoiceComponent>,
    private backend: BackendService,
    private auth: AuthService,
    private cahce: CacheService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder) {}


  createJson() {

    const values = this.cellRefs.toArray().map(cellRef => cellRef.nativeElement.textContent);
    const date = values[0];
    const from = values.slice(1, 10).reduce(toObject('contactPerson', 'name', 'address', 'city', 'state', 'country', 'postalCode', 'phone', 'idNo'), {});
    const to = values.slice(10, 19).reduce(toObject('contactPerson', 'name', 'address', 'city', 'state', 'country', 'postalCode', 'phone', 'fax'), {});
    const [purposeOfShipment, madeIn, awb, signature] = values.slice(19, 23);
    const packages = this.invoiceTable.value;
    const processor = this.processorComboBox.value;

    return {
      date,
      from,
      to,
      purposeOfShipment,
      madeIn,
      awb,
      signature,
      packages,
      processor
    };
  }

  async saveInvoice() {
    this.removeUnusedRows();
    const json = this.createJson();
    const user = this.auth.currentUser.value;

    const payload = {
      pickupId: this.pickupId || null,
      sessionId: user ? user.sessionId : null,
      json,
    };

    return await this.backend.execute('UpdateInvoice', payload);
  }

  printInvoice() {
    this.printing = true;
    this.dialogRef.close();
    setTimeout(() => {
      printJS({
        printable: 'invoice_template',
        type: 'html',
        targetStyles: '*',
        documentTitle: 'Commericial Invoice',
      });
      this.printing = false;
    });
  }

  async saveAndPrint() {
    this.requesting = true;
    try {
      await this.saveInvoice();
      this.requesting = false;
      this.printInvoice();
    } catch {
      this.requesting = true;
    }
  }

  removeUnusedRows() {

    for (let i = 0, groups = this.invoiceTable.controls as FormGroup[]; i < groups.length; i++) {
      const isEmpty = this.isEmpty(groups[i]);
      if (isEmpty && groups.length > 1) {
        this.invoiceTable.removeAt(i);
        i--;
      }
    }
  }

  get invoiceTable() {
    return this.invoiceTableForm.get('invoiceTable') as FormArray;
  }

  getTotal(colName: string) {
    return this.invoiceTable.controls.reduce((acc, curr) => {
      return acc += parseFloat(curr.get(colName).value || 0);
    }, 0);
  }

  isEmpty(group: FormGroup): boolean {
    return Object.values(group.controls).every(control => !control.value);
  }

  isFull(): boolean {
    return this.invoiceTable.controls.every((control: FormGroup) => this.isEmpty(control) === false);
  }


  appendRow() {
    if (this.isFull()) {
      this.invoiceTable.push(this.fb.group(TABLE_ROW_TEMPLATE));
    }
  }

  ngOnInit() {

    const invoice = this.data.invoice;
    console.log(this.data, ' <<<<');

    if (invoice) {
      const {purposeOfShipment, awb, madeIn, signature} = invoice;
      this.from = invoice.from;
      this.to = invoice.to;
      this.date = moment(invoice.date).format('YYYY/MM/DD');
      this.currency = invoice.currency;
      this.pickupId = this.data.pickupId;
      this.processorName = invoice.processor;
      this.misc = {purposeOfShipment, awb, madeIn, signature};

      this.invoiceTableForm = this.fb.group({
        invoiceTable: this.fb.array(invoice.packages.map(pckg => this.fb.group(pckg)))
      });
      this.appendRow();
    } else {

      this.from = this.data.shipperData;
      this.to = this.data.consigneeData;
      this.date = this.data.date || moment().format('YYYY/MM/DD');
      this.currency = this.data.currency;
      this.pickupId = this.data.pickupId;
      this.processorId = this.data.processorId;
      this.misc = {purposeOfShipment: this.data.documentDescription};

      this.invoiceTableForm = this.fb.group({
        invoiceTable: this.fb.array([
          this.fb.group(TABLE_ROW_TEMPLATE),
          this.fb.group(TABLE_ROW_TEMPLATE),
          this.fb.group(TABLE_ROW_TEMPLATE),
        ])
      });
    }

    this.processorComboBox = new FormControl('');

    this.cahce.getProcessorType().then(processors  => {
      processors = processors as Processor[];

      this.processors = processors.filter((processor, index, self) => {
        return self.findIndex(proc => proc.parent === processor.parent) === index;
      });

      let initialProcessor = null;
      if (this.processorId) {
        initialProcessor = processors.find(proc => proc.processorId === this.processorId).parent;
      } else if (this.processorName) {
        initialProcessor = this.processorName;
      }

      this.processorComboBox.setValue(initialProcessor);
    });

  }

}
