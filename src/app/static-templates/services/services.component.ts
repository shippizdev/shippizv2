import { SLIDES } from '@app/constants';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  sliderData = SLIDES;

  constructor() { }

  ngOnInit() {
  }
}
