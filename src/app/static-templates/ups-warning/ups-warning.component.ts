import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-ups-warning',
  templateUrl: './ups-warning.component.html',
  styleUrls: ['./ups-warning.component.scss']
})
export class UpsWarningComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {}

}
