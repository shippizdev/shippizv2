import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InvoiceManagerService } from '@app/services/invoice-manager.service';

@Component({
  selector: 'app-invoice-page',
  templateUrl: './invoice-page.component.html',
  styleUrls: ['./invoice-page.component.scss'],
})
export class InvoicePageComponent implements OnInit {

  loading = true;
  data: any;
  constructor(
    private activeRoute: ActivatedRoute,
    private invoiceMng: InvoiceManagerService) { }

  async ngOnInit() {
    this.openInvoice();
  }

  async openInvoice() {
    this.loading = true;

    const pickupId = parseInt(this.activeRoute.snapshot.paramMap.get('pickupId'));
    await this.invoiceMng.openInvoice(pickupId);
    setTimeout(() => this.loading = false, 300)
    
  }

}
