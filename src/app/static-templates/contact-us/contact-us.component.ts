import { isEmail } from './../../utils/validators';
import { RowInsertion } from './../../animations/row-insertion';
import { BackendService } from './../../services/backend.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

const COORDINATES = [
  {lat: 32.068870, lng: 34.794090},
  {lat: 47.521550, lng: 19.106190},
  {lat: 27.283530, lng: -82.336920},
  {lat: 40.648300, lng: -73.912500}
];

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss'],
  animations: [RowInsertion]
})
export class ContactUsComponent implements OnInit {

  zoom = 2;
  coordinates = COORDINATES;
  selectedLocationIndex: number;

  errorMsg: string;
  successMsg: string;
  isSubmitted = false;
  contactForm = this.fb.group({
    fName: ['', Validators.required],
    lName: ['', Validators.required],
    email: ['', [Validators.required, isEmail]],
    phone: ['', Validators.required],
    subject: [''],
    message: ['']
  });

  @ViewChild('map', {read: ElementRef, static: false})
  map: ElementRef;

  constructor(private fb: FormBuilder, private backend: BackendService) { }

  submit() {
    this.isSubmitted = true;
    if (this.contactForm.valid) {
    const payload = this.contactForm.value;
    this.backend.execute('InsertContact', {...payload, ip: '123.33.33.33'})
      .then(res => this.successMsg = res.data[0][0].instruction)
      .catch(err => this.errorMsg = err);
    }
  }

  reset() {
    this.selectedLocationIndex = null;
    this.zoom = 2;
  }

  selectLocation(index: number) {
    if (index === this.selectedLocationIndex) {
      return this.reset();
    }
    this.selectedLocationIndex = index;
    this.zoom = 16;
    this.map.nativeElement.scrollIntoView({behavior: 'smooth'});
  }

  ngOnInit() {
    // this.selectedLocationIndex = 0;
  }

}
