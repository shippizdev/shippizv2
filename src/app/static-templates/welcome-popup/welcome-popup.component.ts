import { Component, OnInit } from '@angular/core';
import { AuthFormStateObserver } from '@app/utils/observers';

@Component({
  selector: 'app-welcome-popup',
  templateUrl: './welcome-popup.component.html',
  styleUrls: ['./welcome-popup.component.scss']
})
export class WelcomePopupComponent implements OnInit {

  constructor() {}

  openRegistration() {
    AuthFormStateObserver.next({mode: 'registration', highlight: true});
  }

  ngOnInit() {
  }

}
