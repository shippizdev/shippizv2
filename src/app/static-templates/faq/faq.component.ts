import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FAQComponent implements OnInit {

  scrollToContent(content: HTMLDivElement) {
    content.scrollIntoView({
      block: 'start',
      behavior: 'smooth',
    });
  }

  constructor() { }

  ngOnInit() {
  }

}
