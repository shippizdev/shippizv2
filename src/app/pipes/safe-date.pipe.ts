import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import moment from 'moment';

@Pipe({
  name: 'safeDate'
})

export class SafeDatePipe extends DatePipe implements PipeTransform {

  transform(value: string, defaultOutput?: string): string | never {

    try {
      return super.transform(moment(value).format('YYYY-MM-DD'));
    } catch {
      return defaultOutput || value;
    }
  }
}
