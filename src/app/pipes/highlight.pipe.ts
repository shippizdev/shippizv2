import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'highlight'
})

export class HighlightPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(value: string, input: string): any {

    if (input && value) {
      value = value.toString().trim();
      const startIndex = value.toLowerCase().indexOf(input.toLowerCase().trim());
      if (startIndex !== -1) {
        const endLength = input.trim().length;
        const matchingString = value.substr(startIndex, endLength);
        return this.sanitizer.bypassSecurityTrustHtml(
          value.replace(matchingString, '<strong>' + matchingString + '</strong>')
        );
      }
    }
    return value;
  }

}
