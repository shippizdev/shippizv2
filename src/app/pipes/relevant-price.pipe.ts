import { Pipe, PipeTransform } from '@angular/core';
import { CompareResponseData } from '@app/interfaces';
import { CurrencyObserver } from '@app/utils/observers';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'relevantPrice'
})

export class RelevantPricePipe implements PipeTransform {

  transform(processor: CompareResponseData, priceType: string, fixedCurrency?: string): Observable<number> {

    if (fixedCurrency) {
      return of(processor[priceType + '_' + fixedCurrency]);
    }

    return CurrencyObserver.pipe(
      map(({name}) => {
        return processor[priceType + '_' + name]
      })
    )
  }

}
