import { Pipe, PipeTransform } from '@angular/core';
import { CacheService } from '@app/services';
import { PackageType } from '@app/interfaces';

@Pipe({
  name: 'packageType'
})

export class PackageTypePipe implements PipeTransform {

  constructor(private cache: CacheService) {}

  async transform(id: any): Promise<string> {

    const packageType = await this.cache.getShippingType(id) as PackageType;
    return packageType.name;

  }

}
