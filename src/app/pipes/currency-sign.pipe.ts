import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencySign'
})
export class CurrencySignPipe implements PipeTransform {

  transform(name: string): string {
    const tmp = document.createElement('span');
    switch (name) {
        case 'USD':
        tmp.innerHTML = '&#x24;';
        break;
        case 'EUR':
        tmp.innerHTML = '&#x20AC;';
        break;
        case 'NIS':
        tmp.innerHTML = '&#x20AA;';
        break;
    };

    return tmp.innerHTML;
  }

}
