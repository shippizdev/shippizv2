import { Pipe, PipeTransform } from '@angular/core';
import { Country } from '@app/interfaces';
import { CacheService } from '@app/services';

@Pipe({
  name: 'countryName',
})

export class CountryNamePipe implements PipeTransform {

  countries: Country[];
  constructor(private cache: CacheService) {}

  async transform(countryCode: string): Promise<string> {

    if (!countryCode) {
      return Promise.resolve('');
    }

    this.countries = await this.cache.getCountries() as Country[];
    const country = this.countries.find(country => country.countryCode === countryCode)
    return country ? country.name : countryCode;

  }

}
