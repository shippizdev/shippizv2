import { Pipe, PipeTransform } from '@angular/core';
import { CacheService } from '@app/services';
import { Processor } from '@app/interfaces';

@Pipe({
  name: 'processorLogo'
})

export class ProcessorLogoPipe implements PipeTransform {

  constructor(private cache: CacheService) {}

  async transform(id: string | number): Promise<string> {

    const processor = await this.cache.getProcessorType(+id) as Processor;
    return 'assets/icons/' + processor.logo;
  }

}
