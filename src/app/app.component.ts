import { Component, OnInit, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material';
import { devLanguageSwitcher } from '@app/development';
import { AuthService, LocalStorageService } from '@app/services';
import { LanguageObserver } from '@app/utils/observers';
import { TranslateService } from '@ngx-translate/core';

import { API } from './classes';
import { DEFAULT_LANGUAGE, ENVIRONMENT_TOKEN, LANGUAGE_TOKEN } from './constants';
import { Language } from './interfaces';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  constructor(
    translate: TranslateService,
    renderer: Renderer2,
    store: LocalStorageService,
    auth: AuthService,
    dialog: MatDialog
  ) {
    const env = store.get<"dev" | "local" | "prod">(ENVIRONMENT_TOKEN) || "dev";
    API.setEnvironment(env);

    LanguageObserver.next(
      store.get<Language>(LANGUAGE_TOKEN) || DEFAULT_LANGUAGE
    );

    translate.addLangs(["en", "he"]);
    translate.setDefaultLang("en");
    LanguageObserver.subscribe((lang) => {
      translate.use(lang.code);
      // renderer.setAttribute(document.documentElement, 'dir', lang.isRtl ? 'rtl' : 'ltr');
      store.set<Language>(LANGUAGE_TOKEN, lang);
    });

    // auth.currentUser.pipe(take(1))
    // .subscribe(user => {
    //   const lastVisit = store.get<number>(LAST_VISIT_TOKEN);
    //   const passed = Date.now() - lastVisit;
    //   if (user === null && passed > WELCOME_POPUP_TIMEOUT) {
    //     dialog.open(WelcomePopupComponent, {
    //       width: '600px',
    //     });
    //     store.set(LAST_VISIT_TOKEN, Date.now())
    //   }
    // })
  }

  ngOnInit() {
    devLanguageSwitcher();
  }
}
