import { NgModule } from '@angular/core';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TranslatorModule } from '@app/translator';
import { AppModule } from './app.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [

    AppRoutingModule,
    HttpClientModule,
    TranslatorModule,
    AppModule,
    BrowserTransferStateModule,
    BrowserAnimationsModule
  ],

  providers: [],
  bootstrap: [AppComponent]

})

export class AppBrowserModule { }
