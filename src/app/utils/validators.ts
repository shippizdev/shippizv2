import { ValidatorFn, FormGroup, ValidationErrors, FormControl } from '@angular/forms';
import validator from 'validator';

export const passwordConfirm: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('password');
    const confirm = control.get('passwordConfirm');

    return password.value === confirm.value ? null : { dismatch: true };
};

export const isEmail: ValidatorFn = (control: FormControl): ValidationErrors | null => {

  return !control.value || validator.isEmail(control.value) ? null : {invalidEmail: true};
};
