
function combine(value: string): string {
  return value.split(' ').join('');
}

function insensitiveEqual(src: string | number, target: string | number) {
  return src.toString().toLowerCase().trim() === target.toString().toLowerCase().trim();
}

function simpleParse(obj: any, ...keys: string[]): any {
  keys.forEach(key => {
    obj[key] = JSON.parse(obj[key]);
  });

  return obj;
}

function toObject(...keys: string[]) {
      return (acc, curr, index) => {
        acc[keys[index]] = curr;
        return acc;
      };
    }

export {combine, insensitiveEqual, simpleParse, toObject};
