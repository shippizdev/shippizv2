import { BehaviorSubject } from 'rxjs';
import { Language, AuthFormState, PaymentForm } from '@app/interfaces';
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const LanguageObserver = new BehaviorSubject<Language>(null);
const AuthFormStateObserver = new BehaviorSubject<AuthFormState>({mode: null});
const AuthHeader = new BehaviorSubject<HttpHeaders>(new HttpHeaders());
const PaymentAutofill = new BehaviorSubject<PaymentForm>(null);

const CurrencyHandler = new BehaviorSubject<string>('USD');
const CurrencyObserver = CurrencyHandler.pipe(
    map(name => {

        const tmp = document.createElement('span');
        switch (name) {
            case 'USD':
            tmp.innerHTML = '&#x24;';
            break;
            case 'EUR':
            tmp.innerHTML = '&#x20AC;';
            break;
            case 'NIS':
            tmp.innerHTML = '&#x20AA;';
            break;
        };

        return {
            name,
            sign: tmp.innerHTML
        }
        
    })
)

export { LanguageObserver, AuthFormStateObserver, AuthHeader, PaymentAutofill, CurrencyHandler, CurrencyObserver };