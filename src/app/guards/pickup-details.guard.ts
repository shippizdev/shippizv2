import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { LocalStorageService } from '@app/services';
import { ORDER_DATA_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN, LOCATIONS_TOKEN } from '@app/constants';

@Injectable({
  providedIn: 'root'
})
export class PickupDetailsGuard implements CanActivate  {

  constructor(private store: LocalStorageService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.store.get(ORDER_DATA_TOKEN) || !this.store.get(LOCATIONS_TOKEN) || !this.store.get(SELECTED_PROCESSOR_DATA_TOKEN)) {
      this.router.navigate(['/home']);
      return false;
    } else {
      return true;
    }
  }
}
