import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LocalStorageService } from '@app/services';
import { PICKUP_DATA_TOKEN, SELECTED_PROCESSOR_DATA_TOKEN } from '@app/constants';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationGuard implements CanActivate {

  constructor(private store: LocalStorageService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.store.get(PICKUP_DATA_TOKEN) || !this.store.get(SELECTED_PROCESSOR_DATA_TOKEN)) {
      this.router.navigate(['/home']);
      return false;
    } else {
      return true;
    }
  }

}
