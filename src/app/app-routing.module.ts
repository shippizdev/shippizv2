
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  NotFoundPageComponent,
  AboutUsContentComponent,
  AfterOrderingComponent,
  BeforeOrderingComponent,
  ContactUsComponent,
  AboutUsComponent,
  BusinessComponent,
  FAQComponent,
  ServicesComponent,
  AboutShippizComponent
} from '@app/static-templates';

import { ForgotPasswordComponent } from '@app/components';

import {
  HomePageComponent,
  ShipPageComponent,
  ServiceSelectionComponent,
  PickupDetailsComponent,
  PaymentDetailsComponent,
  ConfirmationPageComponent,
  EmailVerificationComponent,
  ProfileComponent,
  MyDeliverysComponent,
  BusinessExcelComponent,
  MyContactsComponent,
  MyStoreComponent,
  SettingsComponent,
  MyInformationsComponent,
  CreditCardComponent,
  OptionsComponent,
} from '@app/views';
import { InvoicePageComponent } from './static-templates/invoice-page/invoice-page.component';
import { ServiceSelectionGuard } from './guards/service-selection.guard';
import { PickupDetailsGuard } from './guards/pickup-details.guard';
import { ConfirmationGuard } from './guards/confirmation.guard';
import { ExternalPickupFormComponent } from './components/external-pickup-form/external-pickup-form.component';
import { ExternalPickupFormHebrewComponent } from './components/external-pickup-form hebrew/external-pickup-form-hebrew.component';
import { DomesticPickupILComponent } from './views/ship-page/domestic-pickup-il/domestic-pickup-il.component';


const routes: Routes = [
  {path: 'pickupform', component: ExternalPickupFormComponent},
  {path: 'pickupform/:token/:id', component: ExternalPickupFormComponent},

  {path: 'ydmpickupform', component: ExternalPickupFormHebrewComponent},
  {path: 'ydmpickupform/:token/:id', component: ExternalPickupFormHebrewComponent},

  {path: 'home', component: HomePageComponent},
  {path: 'ship', component: ShipPageComponent, children: [
    {path: 'service_selection', component: ServiceSelectionComponent, data: {step: 1}, canActivate: [ServiceSelectionGuard]},
    {path: 'address_details', component: PickupDetailsComponent, data: {step: 2}, canActivate: [PickupDetailsGuard]},
    {path: 'domestic_pickup_IL', component: DomesticPickupILComponent, data: {step: 2}},
    {path: 'payment_details/:pickupId', component: PaymentDetailsComponent, data: {step: 3}},

    {path: '', redirectTo: 'service_selection', pathMatch: 'full'},
  ]},

  {path: 'confirmation', component: ConfirmationPageComponent, canActivate: [ConfirmationGuard]},
  {path: 'en/changepassword', component: ForgotPasswordComponent},
  {path: 'invoice/:pickupId', component: InvoicePageComponent},
  {path: 'invoice', component: InvoicePageComponent},


  {path: 'profile', component: ProfileComponent, children: [
    {path: 'my_deliveries', component: MyDeliverysComponent},
    {path: 'business_excel', component: BusinessExcelComponent},
    {path: 'my_contacts', component: MyContactsComponent},
    {path: 'my_store', component: MyStoreComponent},
    {path: 'settings', component: SettingsComponent, children: [
      {path: 'my_informations', component: MyInformationsComponent},
      {path: 'credit_card', component: CreditCardComponent},
      {path: 'options', component: OptionsComponent},
      {path: '', redirectTo: 'my_informations', pathMatch: 'full'},
    ]},
    {path: '', redirectTo: 'my_deliveries', pathMatch: 'full'},
  ]},

  {path: 'services', component: ServicesComponent},
  {path: 'faq', component: FAQComponent, children: [
    {path: 'about-shippiz', component: AboutShippizComponent},
    {path: 'before-ordering', component: BeforeOrderingComponent},
    {path: 'after-ordering', component: AfterOrderingComponent},
    {path: '', redirectTo: 'about-shippiz', pathMatch: 'full'}

  ]},
  {path: 'business', component: BusinessComponent},
  {path: 'about_us', component: AboutUsComponent, children: [
    {path: '', component: AboutUsContentComponent}
  ]},
  {path: 'contact_us', component: ContactUsComponent},
  {path: 'emailverification', component: EmailVerificationComponent},

  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: NotFoundPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
